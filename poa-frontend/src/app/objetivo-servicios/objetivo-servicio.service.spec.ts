import { TestBed } from '@angular/core/testing';

import { ObjetivoServicioService } from './objetivo-servicio.service';

describe('ObjetivoServicioService', () => {
  let service: ObjetivoServicioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObjetivoServicioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
