import { CronogramaAnual } from './CronogramaAnual';
import { Actividad } from './Actividad';
import { Usuario } from './Usuario';

export class Objetivo {
    id?: number;
    codigoobjetivo: string;
    gestion: string;
    area: string;
    sede: string;
    objetivogestion: string;
    productoresultadoobjetivo: string;
    meses: Array<CronogramaAnual>;
    indicadorobjetivo: string;
    medioverificacionobjetivo: string;
    estado: number;
    fechahoraregistro: Date;
    codigousuario?: string;
    actividades: Array<Actividad>;
    usuario: Usuario;
}
