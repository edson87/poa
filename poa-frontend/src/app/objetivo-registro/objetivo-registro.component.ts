import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CronogramaAnual } from '../Modelos/CronogramaAnual';
import { Objetivo } from '../Modelos/Objetivo';
import { ActivatedRoute, Router } from '@angular/router';
import { Token } from '../Modelos/token';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { Usuario } from '../Modelos/Usuario';
import  Swal  from 'sweetalert2';


@Component({
  selector: 'app-objetivo-registro',
  templateUrl: './objetivo-registro.component.html',
  styleUrls: ['./objetivo-registro.component.css']
})
export class ObjetivoRegistroComponent implements OnInit {

  @Output() actualizar = new EventEmitter<boolean>();
  formObjetivo: FormGroup;
  formCronograma: FormGroup;
  cronograma: CronogramaAnual;
  objetivo: Objetivo;
  listaCronograma: Array<CronogramaAnual> = [];
  public usuario: Token;
  public gestion: string;
  listaMesesAux: Array<any> = [];

  constructor(private route: ActivatedRoute, public servicio: ObjetivoServicioService,
              private router: Router) {
    this.cronograma = new CronogramaAnual();
    this.objetivo = new Objetivo();
    this.listaCronograma = Array<CronogramaAnual>();
    this.usuario = new Token();
  }

  ngOnInit(): void {
    this.inicializarFormularioObjetivo();
    this.inicializarCronogramaMesual();
  }

  public inicializarFormularioObjetivo(): void {
    this.formObjetivo = new FormGroup({
      codigoobjetivo: new FormControl('', {
        validators: [Validators.required]
      }),
      objetivogestion: new FormControl('', {
        validators: [Validators.required]
      }),
      productoresultadoobjetivo: new FormControl('', {
        validators: [Validators.required]
      }),
      meses: new FormControl('', {
        validators: [Validators.required]
      }),
      indicadorobjetivo: new FormControl('', {
        validators: [Validators.required]
      }),
      medioverificacionobjetivo: new FormControl('', {
        validators: [Validators.required]
      })
    });
  }

  inicializarCronogramaMesual(): void{
    this.formCronograma = new FormGroup({
      enero: new FormControl('', {
        validators: []
      }),
      febrero: new FormControl('', {
        validators: []
      }),
      marzo: new FormControl('', {
        validators: []
      }),
      abril: new FormControl('', {
        validators: []
      }),
      mayo: new FormControl('', {
        validators: []
      }),
      junio: new FormControl('', {
        validators: []
      }),
      julio: new FormControl('', {
        validators: []
      }),
      agosto: new FormControl('', {
        validators: []
      }),
      septiembre: new FormControl('', {
        validators: []
      }),
      octubre: new FormControl('', {
        validators: []
      }),
      noviembre: new FormControl('', {
        validators: []
      }),
      diciembre: new FormControl('', {
        validators: []
      })
    });
  }


  guardarObjetivo(): void {
    const usuario = new Usuario();
    const meses = Object.values(this.formCronograma.value);
    for (let index = 0; index < meses.length; index++) {
      const element = meses[index];

      if (element === true){
        this.cronograma.mes = this.mesesLiteral(index);
        this.listaCronograma.push(this.cronograma);
        this.cronograma = new CronogramaAnual();
      }
    }
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    const permiso = sessionStorage.getItem('permisos');
    const permisos = JSON.parse(window.atob(permiso));


    this.objetivo.codigoobjetivo = this.formObjetivo.value.codigoobjetivo;
    this.objetivo.objetivogestion = this.formObjetivo.value.objetivogestion;
    this.objetivo.gestion = this.usuario.gestion;
    this.objetivo.area = this.usuario.area;
    this.objetivo.sede = this.usuario.sede;
    this.objetivo.productoresultadoobjetivo = this.formObjetivo.value.productoresultadoobjetivo;
    this.objetivo.meses = this.listaCronograma;
    this.objetivo.indicadorobjetivo = this.formObjetivo.value.indicadorobjetivo;
    this.objetivo.medioverificacionobjetivo = this.formObjetivo.value.medioverificacionobjetivo;
    this.objetivo.estado = 1;
    this.objetivo.fechahoraregistro = new Date();
    this.objetivo.codigousuario = this.usuario.usuario;
    usuario.codigousuario = this.usuario.usuario;
    usuario.permisos = permisos;
    this.objetivo.usuario = usuario;
    console.log(this.objetivo)
    this.servicio.guardarObjetivoDeLaGestion(this.objetivo).subscribe(
      (response) => {

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `${response.mensaje}`,
          showConfirmButton: false,
          timer: 2000
        });

        this.objetivo = new Objetivo();
        this.formObjetivo.reset();
        this.formCronograma.reset();
        this.cerrarModal();
        this.actualizar.emit(true);

      }, (err) => {
        console.log(err.error);
        Swal.fire({
          icon: 'error',
          title: '',
          text: `${err.mensaje}`,
          footer: ''
        })
      }
    );

  }

  public mesesLiteral(numero: number): string{
    let mes: any = '';
    if (numero === 0) {
      mes = 'Enero';
    } else if (numero === 1) {
      mes = 'Febrero';
    } else if (numero === 2) {
      mes = 'Marzo';
    } else if (numero === 3) {
      mes = 'Abril';
    } else if (numero === 4) {
      mes = 'Mayo';
    } else if (numero === 5) {
      mes = 'Junio';
    } else if (numero === 6) {
      mes = 'Julio';
    } else if (numero === 7) {
      mes = 'Agosto';
    } else if (numero === 8) {
      mes = 'Septiembre';
    } else if (numero === 9) {
      mes = 'Octubre';
    } else if (numero === 10) {
      mes = 'noviembre';
    } else if (numero === 11) {
      mes = 'Diciembre';
    }
    return mes;
  }

  cerrarModal(): void {
    this.servicio.cerrarModal();
    this.objetivo = new Objetivo();
    this.cronograma = new CronogramaAnual();
    this.formObjetivo.reset();
    this.formCronograma.reset();
    this.actualizar.emit(true);
  }
}
