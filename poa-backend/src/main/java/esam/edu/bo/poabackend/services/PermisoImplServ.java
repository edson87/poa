package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.IPermisoDao;
import esam.edu.bo.poabackend.web.models.Permiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PermisoImplServ implements IPermisoServices {

    @Autowired
    IPermisoDao permisoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Permiso> getAllPermisos() {
        List<Permiso> permisos = permisoDao.findAll();
        return permisos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Permiso> getPermisosByRol(String rol) {
        List<Permiso> permisos = permisoDao.findPermisoByRol(rol);
        return permisos;
    }

    @Override
    public void updatePermisoByRol(Permiso permiso) {
        //permisoDao.updatePermisos(copiarObjetivo, crearActividad, crearObjetivo, editarActividad, editarObjetivo, eliminarActividad, eliminarObjetivo, leerActividad, leerObjetivo, excelObjetivo, rol);
        permisoDao.save(permiso);
    }

    @Override
    @Transactional(readOnly = true)
    public Permiso getPermisos(String cod_usuario) {
        //Permiso permiso = permisoDao.findPermisoByUsuarioCodigousuario(cod_usuario);
        //return permiso;
        return null;
    }

    @Override
    public Permiso savePermisos(Permiso permiso) {
        return permisoDao.save(permiso);
    }

    @Override
    public Permiso updatePermisos(Permiso permiso) {
        //Permiso updatePermiso = permisoDao.save(permiso);
        //return updatePermiso;
        return null;
    }
}
