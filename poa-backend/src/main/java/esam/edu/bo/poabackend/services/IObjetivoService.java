package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Objetivo;

import java.util.List;

public interface IObjetivoService {
    public List<Objetivo> getAllObjetivos();

    public Objetivo saveObjetivos(Objetivo objetivos);

    public Objetivo getObjetivos(long id, boolean estado);

    public Objetivo updateObjetivos(Objetivo objetivos);

    public void deleteObjetivos(long id);

    public List<String> getAllManagements(String area, boolean estado);

    public List<Objetivo> getObjetiveByManagementAndAreaAndSede(String gestion, String area, boolean estado, String sede);

    public Objetivo getObjetiveByCodObjeAndAreaAndSede(String codObje, String area, boolean estado, String sede);

    public List<Object[]> getAllCodObjetivos(String area, String gestion, boolean estado, String sede);

    public Objetivo getObjetivoByCodUsuario(String cod_usuario);
}
