import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetivoCatalogoComponent } from './objetivo-catalogo.component';

describe('ObjetivoCatalogoComponent', () => {
  let component: ObjetivoCatalogoComponent;
  let fixture: ComponentFixture<ObjetivoCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetivoCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetivoCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
