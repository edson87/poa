
export class Encargado {
    id?: number;
    nombre: string;
    apellidos: string;
    completo: string;
    area: string;
    sede: string;
}
