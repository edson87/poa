import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadCatalogoComponent } from './actividad-catalogo.component';

describe('ActividadCatalogoComponent', () => {
  let component: ActividadCatalogoComponent;
  let fixture: ComponentFixture<ActividadCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
