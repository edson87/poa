package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.CronogramaAnual;
import esam.edu.bo.poabackend.web.models.Objetivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ICronogramaDao extends JpaRepository<CronogramaAnual, Long> {

    //@Query("delete from CronogramaAnual c where c.objetivo = ?1")
    public void deleteCronogramaAnualByObjetivo(Objetivo objetivo);
}
