
export class SubActividad {
    id?: number;
    fechaInicio: Date;
    fechaFin: Date;
    estado: boolean;
    subActividad: string;
    resultadoObjetivo: string;
    subIndicador: string;
    subMediosVerificacion: string;
    presupuesto: number;
    encargado: string;
}
