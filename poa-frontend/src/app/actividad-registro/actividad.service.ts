import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { Actividad } from '../Modelos/Actividad';
import { SubActividad } from '../Modelos/SubActividad';
import { URL_BACKEND } from '../config_url/config';

// borrar
@Injectable({
  providedIn: 'root'
})
export class ActividadService {
  //private urlApi = 'http://localhost:8080/api/actividad';
  private urlApi = `${URL_BACKEND}/api/actividad`;

  constructor(private http: HttpClient) { }

  public resgistrarActividades(actividad: Actividad, area: string): Observable<any> {
    return this.http.post<any>(`${this.urlApi}/guardar/${area}`, actividad);
  }

  public obtenerActividadPorId(id: number): Observable<any>{
    return this.http.get<any>(`${this.urlApi}/${id}`);
  }

  public actualizarActividad(actividad: Actividad): Observable<any>{
    return this.http.put<any>(`${this.urlApi}/actualizar`, actividad);
  }
}
