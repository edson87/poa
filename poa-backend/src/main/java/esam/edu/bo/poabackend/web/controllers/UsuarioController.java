package esam.edu.bo.poabackend.web.controllers;

import esam.edu.bo.poabackend.services.PermisoImplServ;
import esam.edu.bo.poabackend.services.UsuarioImplServ;
import esam.edu.bo.poabackend.web.models.Permiso;
import esam.edu.bo.poabackend.web.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RequestMapping("/api/usuarios")
@RestController
public class UsuarioController {

    @Autowired
    UsuarioImplServ usuarioImpl;

    @Autowired
    PermisoImplServ permisoImpl;

    @GetMapping
    public ResponseEntity<?> getAllUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();
        Map<String, Object> message = new HashMap<>();

        try {
            usuarios = usuarioImpl.getAllUsuarios();

        } catch (DataAccessException e ) {
            message.put("mensaje", "Error al obtener a los usuarios.");
            message.put("error", e.getCause());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(usuarios, HttpStatus.OK);
    }

    @PutMapping("/update/usuario/{cod}")
    public ResponseEntity<?> updatePermisosByCodUsuario(@PathVariable(name = "cod") String codUsuario, @RequestBody Permiso permisos) {
        Permiso nuevoPermiso = new Permiso();
        Usuario usuario =  new Usuario();
        Map<String, Object> message = new HashMap<>();

        try {
            usuario = usuarioImpl.getUsuario(codUsuario);

            if (usuario == null) {
                message.put("mensaje", "No existe el usuario con el codigo de usuario " + codUsuario);
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }

            nuevoPermiso = usuario.getPermisos();

            nuevoPermiso.setLeer_objetivo(permisos.isLeer_objetivo());
            nuevoPermiso.setCrear_objetivo(permisos.isCrear_objetivo());
            nuevoPermiso.setEditar_objetivo(permisos.isEditar_objetivo());
            nuevoPermiso.setEliminar_objetivo(permisos.isEliminar_objetivo());
            nuevoPermiso.setExcel_objetivo(permisos.isExcel_objetivo());
            nuevoPermiso.setCopiar_objetivo(permisos.isCopiar_objetivo());

            nuevoPermiso.setLeer_actividad(permisos.isLeer_actividad());
            nuevoPermiso.setCrear_actividad(permisos.isCrear_actividad());
            nuevoPermiso.setEditar_actividad(permisos.isEditar_actividad());
            nuevoPermiso.setEliminar_actividad(permisos.isEliminar_actividad());

            permisoImpl.savePermisos(nuevoPermiso);


        } catch (DataAccessException e) {
            message.put("mensaje", "No se pudo editar los permisos del usuario " + codUsuario);
            message.put("error", e.getCause().getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "Los permisos se editaron con exito");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
