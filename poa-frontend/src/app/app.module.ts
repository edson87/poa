import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { InicioComponent } from './inicio/inicio.component';
import { ActividadRegistroComponent } from './actividad-registro/actividad-registro.component';
import { ObjetivoRegistroComponent } from './objetivo-registro/objetivo-registro.component';
import { ObjetivoCatalogoComponent } from './objetivo-catalogo/objetivo-catalogo.component';
import { FooterComponent } from './footer/footer.component';
import { ActividadCatalogoComponent } from './actividad-catalogo/actividad-catalogo.component';
import { ObjetivoEditarComponent } from './objetivo-editar/objetivo-editar.component';
import { ActividadEditarComponent } from './actividad-editar/actividad-editar.component';
import { GestionComponent } from './configuracionesMenu/gestion/gestion.component';
import { AreaComponent } from './configuracionesMenu/area/area.component';
//area/area/area.component
// librerias externar
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { VerObjetivoComponent } from './objetivo-ver/ver-objetivo/ver-objetivo.component';
import { SedeComponent } from './configuracionesMenu/sede/sede.component';
import { PermisosComponent } from './configuracionesMenu/permisos/permisos.component';
import { ObjetivoCopiarComponent } from './objetivo-copiar/objetivo-copiar.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    InicioComponent,
    ActividadRegistroComponent,
    ObjetivoRegistroComponent,
    ObjetivoCatalogoComponent,
    FooterComponent,
    ActividadCatalogoComponent,
    ObjetivoEditarComponent,
    ActividadEditarComponent,
    GestionComponent,
    AreaComponent,
    VerObjetivoComponent,
    SedeComponent,
    PermisosComponent,
    ObjetivoCopiarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AutocompleteLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
