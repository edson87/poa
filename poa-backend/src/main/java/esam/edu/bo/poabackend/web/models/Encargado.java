package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

//@Entity
//@Table(name = "encargado")
public class Encargado implements Serializable {
    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;
    private String apellidos;
    private String completo;
    private String area;
    private String sede;

    //@JsonIgnore
   /* @OneToOne
    @JsonIgnoreProperties("encargado")*/
    private SubActividad subActividad;

    public Encargado() {
    }

    public Encargado(String nombre, String apellidos, String completo, String area, String sede, SubActividad subActividad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.completo = completo;
        this.area = area;
        this.sede = sede;
        this.subActividad = subActividad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCompleto() {
        return completo;
    }

    public void setCompleto(String completo) {
        this.completo = completo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public SubActividad getSubActividad() {
        return subActividad;
    }

    public void setSubActividad(SubActividad subActividad) {
        this.subActividad = subActividad;
    }
}
