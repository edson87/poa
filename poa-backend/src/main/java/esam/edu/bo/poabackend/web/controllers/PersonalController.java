package esam.edu.bo.poabackend.web.controllers;

import esam.edu.bo.poabackend.services.PersonalServImpl;
import esam.edu.bo.poabackend.web.models.Personal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/personal")
public class PersonalController {

    @Autowired
    PersonalServImpl personalServ;

    @GetMapping
    public List<Personal> getAllPersonals() {
        List<Personal> personales = personalServ.getAllPersonals();

        return personales;
    }
}
