package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Permiso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IPermisoDao extends JpaRepository<Permiso, Long> {

    //@Modifying
    //@Query("update Permiso p set p.copiar_objetivo = ?1, p.crear_actividad = ?2, p.crear_objetivo = ?3, p.editar_actividad = ?4, p.editar_objetivo = ?5, p.eliminar_actividad = ?6, p.eliminar_objetivo = ?7, p.leer_actividad = ?8, p.leer_objetivo = ?9, p.excel_objetivo = ?10 where p.rol like ?11")
    //public void updatePermisos(boolean copiarObjetivo, boolean crearActividad, boolean crearObjetivo, boolean editarActividad, boolean editarObjetivo, boolean eliminarActividad, boolean eliminarObjetivo, boolean leerActividad, boolean leerObjetivo, boolean excelObjetivo, String rol);

    public List<Permiso> findPermisoByRol(String rol);

    public Permiso findPermisoByUsuarioCodigousuario(String cod_usuario);
}
