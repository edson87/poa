package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.IUsuarioDao;
import esam.edu.bo.poabackend.web.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioImplServ implements IUsuarioService {

    @Autowired
    IUsuarioDao usuarioDao;


    @Override
    @Transactional(readOnly = true)
    public List<Usuario> getAllUsuarios() {
        List<Usuario> usuarios = usuarioDao.findAll();
        return usuarios;
    }

    public Usuario insertUsuario(Usuario usuario) {
       return usuarioDao.save(usuario);
    }

    public Usuario getUsuario(String cod_usuario) {
        Usuario usuario = usuarioDao.findUsuarioByCodigousuario(cod_usuario);
        return usuario;
        //return null;
    }
}
