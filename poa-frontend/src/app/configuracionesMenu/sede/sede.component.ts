import { Component, OnInit } from '@angular/core';
import { Token } from '../../Modelos/token';
import { ConfiguracionService } from '../../configuracionService/configuracion.service';
import { sedes } from '../../Modelos/sedes';


@Component({
  selector: 'app-sede',
  templateUrl: './sede.component.html',
  styleUrls: ['./sede.component.css']
})
export class SedeComponent implements OnInit {
  public listaSedes: any = sedes;
  public sedeSeleccionada: any
  public usuario: Token;


  constructor(private service: ConfiguracionService) { }

  ngOnInit(): void {
    this.sedeSeleccionada = '';
    this.usuario = new Token();
  }


  obtenerToken(): Token {
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  editarToken(usuario: Token): void {
    sessionStorage.removeItem('usuario');
    sessionStorage.setItem('usuario', JSON.stringify(usuario));
  }

  click(sedeSeleccionada) {
    console.log(sedeSeleccionada)
    this.usuario = this.obtenerToken();
    this.usuario.sede = sedeSeleccionada;
    this.editarToken(this.usuario);
    this.mandarToken(this.usuario);
  }

  public mandarToken(token: Token): void {
    this.service.mandarMensajeCambioSede(token);
  }

}
