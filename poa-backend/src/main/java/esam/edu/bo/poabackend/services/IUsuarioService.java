package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Usuario;

import java.util.List;

public interface IUsuarioService {
    public List<Usuario> getAllUsuarios();
    public Usuario insertUsuario(Usuario usuario);
    public Usuario getUsuario(String cod_usuario);
}
