import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../config_url/config';


@Injectable({
  providedIn: 'root'
})
export class PermisosServiciosService {
  //private url = 'http://localhost:8080/api/permisos';
  private url = URL_BACKEND + '/api/permisos';

  constructor(private http: HttpClient) { }


  obtenerPermisosPorUsuario(cod_usuario: string): Observable<any> {
    return this.http.get<any>(`${this.url}/usuario/${cod_usuario}`);
  }

  insertarPermisoPorDefectoAUsuario(permiso: any, cod_usuario: string): Observable<any> {
    return this.http.post<any>(`${this.url}/usuario/${cod_usuario}`, permiso);
  }

}
