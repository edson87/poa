package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Objetivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IObjetivosDao extends JpaRepository<Objetivo, Long> {
    @Query("select o from Objetivo o where o.id = ?1 and o.estado = true")
    public Objetivo queryFindByIdAndEstado(long id, boolean estado);

    @Query("select distinct o.gestion from Objetivo o where o.area = ?1 and o.estado = true")
    public List<String> findAllManagements(String area, boolean estado);

    @Query("select o from Objetivo o where o.gestion like ?1 and o.area like ?2 and o.estado = ?3 and o.sede = ?4")
    public List<Objetivo> findObjetivoByGestionAndAreas(String gestion, String area, boolean estado, String sede);
    //public Objetivo findObjetivoByGestionAndArea(String gestion, String area);

    @Query("select o from Objetivo o where o.codigoobjetivo like ?1 and o.area like ?2 and o.estado = ?3 and o.sede = ?4")
    public Objetivo findObjetivoByCodObjeAndAreaAndSede(String codObje, String area, boolean estado, String sede);

    @Query("select o.codigoobjetivo, o.objetivogestion from Objetivo o where o.area = ?1 and o.gestion = ?2 and o.estado = ?3 and o.sede = ?4")
    public List<Object[]> findAllCodObjetivo(String area, String gestion, boolean estado, String sede);

    public Objetivo findByCodigousuario(String cod_usuario);

}
