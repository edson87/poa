package esam.edu.bo.poabackend.web.models;

import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Data
@Entity
@Table(name = "subactividades")
public class SubActividad implements Serializable {

    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "inicio")
    private Date fechaInicio;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "fin")
    private Date fechaFin;

    private boolean estado;

    @Column(name = "actividad")
    @NotNull
    private String subActividad;

    @Column(name = "resultado")
    @NotNull
    private String resultadoObjetivo;

    @Column(name = "indicador")
    private String subIndicador;

    @Column(name = "medios_verificacion")
    private String subMediosVerificacion;

    private double presupuesto;

    private String encargado;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "actividad_id")
    @JsonIgnoreProperties("actividad")
    private Actividad actividad;

    //@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subActividad")
    //@JsonIgnoreProperties("subActividad")
    //private Encargado encargado;
}
