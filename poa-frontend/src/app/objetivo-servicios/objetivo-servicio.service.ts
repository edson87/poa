import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Objetivo } from '../Modelos/Objetivo';
import { Token } from '../Modelos/token';
import { URL_BACKEND } from '../config_url/config';


@Injectable({
  providedIn: 'root'
})
export class ObjetivoServicioService {
  private menu = new BehaviorSubject<any>(undefined);
  currentmenu = this.menu.asObservable();

  private menu2 = new BehaviorSubject<any>(undefined);
  currentmenu2 = this.menu2.asObservable();

  public modal: boolean = false;
  public modalEditar: boolean = false;
  public modalVer = false;
  public modelCopiar: boolean = false;

  //public urlObjetivos: string = 'http://localhost:8080/api/objetivos';
  public urlObjetivos: string = URL_BACKEND + '/api/objetivos';


  constructor(private http: HttpClient) { }

  public obtenerLasGestiones(area: string): Observable<any> {
    const estado = true;
    return this.http.get<any>(`${this.urlObjetivos}/managements/${area}/${estado}`);
  }

  public obtenerObjetivoPorGestion(gestion: string, area: string, sede: string): Observable<any>{
    const estado = true;
    return this.http.get<Objetivo>(`${this.urlObjetivos}/${gestion}/${area}/${estado}/${sede}`);
  }

  public guardarObjetivoDeLaGestion(objetivo: Objetivo): Observable<any>{
    console.log(objetivo)
    return this.http.post<any>(`${this.urlObjetivos}`, objetivo);
  }

  public obtenerObjetivoPorId(id: number): Observable<any> {
    const estado = true;
    return this.http.get<any>(`${this.urlObjetivos}/${id}/${estado}`);
  }

  public actualizarObjetivos(objetivo: Objetivo): Observable<any> {
    return this.http.put<any>(`${this.urlObjetivos}/${objetivo.id}`, objetivo );
  }

  public eliminarObjetivos(objetivo: Objetivo): Observable<any> {
    return this.http.put<any>(`${this.urlObjetivos}/eliminar`, objetivo);
  }

  public copiarObjetivo(objetivo: Objetivo, sede: string): Observable<any> {
    return this.http.post<any>(`${this.urlObjetivos}/copiar/${sede}`, objetivo);
  }

  public copiarTodosLosObjetivo(objetivos: Array<Objetivo>, sede: string): Observable<any> {
    return this.http.post<any>(`${this.urlObjetivos}/copiar-todos/${sede}`, objetivos);
  }

  public copiarObjetivoPorAño(objetivo: Objetivo, gestion: string): Observable<any> {
    return this.http.post<any>(`${this.urlObjetivos}/copiar/gestion/${gestion}`, objetivo);
  }

  public mandarMensaje(token: Token): void {
    this.menu.next(token);
  }

  public mandarMensajeDesdeObjetivo(token: Token): void {
    this.menu2.next(token);
  }

  public abrirModal(): boolean {
    return this.modal = true;
  }

  public cerrarModal(): boolean {
    return this.modal = false;
  }

  public abrirModalVer(): boolean {
    return this.modalVer = true;
  }

  public cerrarModalVer(): boolean {
    return this.modalVer = false;
  }

  public abrirModelCopiar() :boolean{
    return this.modelCopiar = true;
  }

  public cerrarModalCopiar(): boolean {
    return this.modelCopiar = false;
  }

}
