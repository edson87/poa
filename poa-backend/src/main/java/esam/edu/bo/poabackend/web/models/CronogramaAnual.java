package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name = "cronogramas")
public class CronogramaAnual implements Serializable {
    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String mes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "objetivo_id")
    @JsonIgnoreProperties("objetivo")
    private Objetivo objetivo;
}
