import { Component, OnInit } from '@angular/core';
import { ActividadService } from '../actividad-service/actividad.service';
import { Token } from '../Modelos/token';
import { Actividad } from '../Modelos/Actividad';
import { Router } from '@angular/router';
import  Swal  from 'sweetalert2';


@Component({
  selector: 'app-actividad-catalogo',
  templateUrl: './actividad-catalogo.component.html',
  styleUrls: ['./actividad-catalogo.component.css']
})
export class ActividadCatalogoComponent implements OnInit {
  private usuario: Token;
  public listaActividades: Array<Actividad>;
  public sinRegistro: boolean = false;
  public buscar: string;
  public listaCodigos: Array<any>;
  public codigoSeleccionada: string;
  public permisos: any = {};
  codigos: any;

  constructor(private actividadService: ActividadService, public service: ActividadService,
              private router: Router) {
    this.usuario = new Token();
    this.listaActividades = new Array<Actividad>();
    this.sinRegistro = true;
    this.listaCodigos = [];
  }

  ngOnInit(): void {
    //this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    this.obtenerCodigosObjetivos();
    this.obtenerPermisosUsuario();
  }

  obtenerToken(): Token{
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  obtenerPermisosUsuario(): void {
    const permiso = sessionStorage.getItem('permisos');
    this.permisos = JSON.parse(window.atob(permiso));
    //console.log(this.permisos)
  }

  obtenerCodigosObjetivos(): void {
    this.usuario = this.obtenerToken();

    const area = this.usuario.area;
    const gestion = this.usuario.gestion;
    const sede = this.usuario.sede;
    this.service.obtenerListaCodigosObjetivos(area, gestion, sede).subscribe(
      (response) => {
        let arreglo = [];
        for (let i = 0; i < response.length; i++) {
          const element = response[i];
          arreglo['codigo'] = element[0];
          arreglo['objetivo'] = element[1].substring(0, 60) + '...';
          this.listaCodigos.push(arreglo);
          arreglo = [];
        }
      }, (err) => {
        console.log('no exiten codigos', err);
      }
    );
  }

  public abrirModal(): void {
    this.service.abrirModal();
    this.codigoSeleccionada = null;
    this.listaActividades = new Array<Actividad>();
    this.sinRegistro = true;
  }

  obtenerCodObj(codigoSeleccionada): void{
    const codigo = codigoSeleccionada.codigo;

    if (codigo !== '') {
      this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
      this.listaActividades = new Array<Actividad>();
      const area = this.usuario.area;
      const sede = this.usuario.sede;
      this.actividadService.obtenenerActividadPorCodObjeYAreaYSede(codigo, area, sede).subscribe(
        (response) => {
          //console.log(response)
          this.listaActividades = response;

          this.sinRegistro = false;
          this.buscar = '';
        }, (err) => {
          console.log(err);
          this.sinRegistro = true;
          this.buscar = '';
        }
      );
    } else {
      this.listaActividades = new Array<Actividad>();
    }
  }

  editarActividad(actividad): void{
    this.router.navigate(['/actividad/editar', actividad.id]);
  }

  eliminarActividad(actividad): void {
    Swal.fire({
      title: 'Es seguro de querer eliminar la actividad?',
      text: "",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Aceptar'
    }).then((result) => {

      if (result.isConfirmed) {
        this.service.eliminarActividad(actividad).subscribe(
          (response) => {
            console.log(response);
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: `${response.mensaje}`,
              showConfirmButton: false,
              timer: 2000
            });
            this.obtenerCodObj(this.codigoSeleccionada);
          }
        ), (err) => {
          console.log(err.error)
          Swal.fire({
            icon: 'error',
            title: '',
            text: `${err.mensaje}`,
            footer: ''
          })
        }

      }

    })

  }
}
