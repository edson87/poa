import { Component, OnInit } from '@angular/core';
import { Actividad } from '../Modelos/Actividad';
import { SubActividad } from '../Modelos/SubActividad';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Token } from '../Modelos/token';
import { ActividadService } from '../actividad-registro/actividad.service';
import { ActividadService as ActividadServ } from '../actividad-service/actividad.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-actividad-editar',
  templateUrl: './actividad-editar.component.html',
  styleUrls: ['./actividad-editar.component.css']
})
export class ActividadEditarComponent implements OnInit {

  public actividades: Actividad;
  public subActividad: SubActividad;
  public listaSubAct: Array<SubActividad> = [];
  public listaSubActEliminadas: Array<SubActividad> = [];
  public formActividad: FormGroup;
  public formSubActividad: FormGroup;
  public agregar: boolean = false;
  public index = 0;
  public usuario: Token;
  fechaIngresada: Date;

  public keyword = 'completo';
  public data$: Observable<any>;

  constructor(private router: Router, private route: ActivatedRoute,
              private service: ActividadService, private service2: ActividadServ) {
                this.actividades = new Actividad();
                this.subActividad = new SubActividad();
                this.listaSubAct = new Array<SubActividad>();
                this.usuario = new Token();
              }

  ngOnInit(): void {
    this.inicializarActividad();
    this.inicializarActividad();
    this.obtenerListaPersonales();

    this.route.params.subscribe(
      (params) => {
        const id = Number(params['id']);

        if (id) {
          this.service.obtenerActividadPorId(id).subscribe(
            (response) => {
              //console.log(response);
              this.actividades = response;
              //console.log(this.actividades)
              this.fechaIngresada = this.actividades.fechaIngresada;
              this.inicializarActividad();

              this.listaSubAct = response.subActividades.map((value) => {
                  return value;
              });
          });
        }
        return;
        this.router.navigate(['/actividad/catalogo']);
      });
  }

  inicializarActividad(): void {
    this.formActividad = new FormGroup({
      codObjetivo: new FormControl(this.actividades.codObjetivo, {
        validators: [Validators.required]
      }),
      actividad: new FormControl(this.actividades.actividad, {
        validators: [Validators.required]
      }),
      resultado: new FormControl(this.actividades.resultado, {
        validators: [Validators.required]
      })
    });
  }

  public iniciarSubActividad(): void {
    this.formSubActividad = new FormGroup({
      fechaInicio: new FormControl('', {
        validators: [Validators.required]
      }),
      fechaFin: new FormControl('', {
        validators: [Validators.required]
      }),
      subActividad: new FormControl('', {
        validators: [Validators.required]
      }),
      resultadoObjetivo: new FormControl('', {
        validators: [Validators.required]
      }),
      subIndicadores: new FormControl('', {
        validators: [Validators.required]
      }),
      subMediosVerificacion: new FormControl('', {
        validators: [Validators.required]
      }),
      presupuesto: new FormControl('', {
        validators: [Validators.required]
      }),
      encargado: new FormControl('', {
        validators: [Validators.required]
      })
    });
  }

  public agregarSubActividad(): void{
    if (this.agregar === false) {
      this.agregar = true;
      this.iniciarSubActividad();
    }
  }

  cancelarSubActividad(): void {
    if (this.agregar === true) {
      this.agregar = false;
      this.formSubActividad.reset();
    }
  }

  obtenerListaPersonales(): void {
    //this.data$ = this.service.obtenerListaPersonales();

    this.service2.obtenerListaPersonales().subscribe(
      (response) => {

        this.data$ = response;

    }, (err) => {
      console.log('No hay objetivos registrados');
      console.log(err);
    });
  }

  quitarSubActividad(subAct: SubActividad): void {
    this.listaSubAct = this.listaSubAct.filter(act => act !== subAct);
    subAct.estado = false;
    this.listaSubActEliminadas.push(subAct);
  }

  editarSubActividad(subAct): void {
    this.agregar = true;
    this.formSubActividad = new FormGroup({
      id: new FormControl(subAct.id, {
        validators: []
      }),
      fechaInicio: new FormControl(subAct.fechaInicio, {
        validators: [Validators.required]
      }),
      fechaFin: new FormControl(subAct.fechaFin, {
        validators: [Validators.required]
      }),
      subActividad: new FormControl(subAct.subActividad, {
        validators: [Validators.required]
      }),
      resultadoObjetivo: new FormControl(subAct.resultadoObjetivo, {
        validators: [Validators.required]
      }),
      subIndicadores: new FormControl(subAct.subIndicador, {
        validators: [Validators.required]
      }),
      subMediosVerificacion: new FormControl(subAct.subMediosVerificacion, {
        validators: [Validators.required]
      }),
      presupuesto: new FormControl(subAct.presupuesto, {
        validators: [Validators.required]
      }),
      encargado: new FormControl('', {
        validators: [Validators.required]
      })
    });
    this.listaSubAct = this.listaSubAct.filter(act => act !== subAct);
  }

  public guardarSubActividad(): void{
    const formSubAct = this.formSubActividad.value;
    this.subActividad.id = formSubAct.id;
    this.subActividad.fechaInicio = formSubAct.fechaInicio;
    this.subActividad.fechaFin = formSubAct.fechaFin;
    this.subActividad.estado = true;
    this.subActividad.subActividad = formSubAct.subActividad;
    this.subActividad.resultadoObjetivo = formSubAct.resultadoObjetivo;
    this.subActividad.subIndicador = formSubAct.subIndicadores;
    this.subActividad.subMediosVerificacion = formSubAct.subMediosVerificacion;
    this.subActividad.presupuesto = formSubAct.presupuesto;

    if (formSubAct.encargado.completo) {
      this.subActividad.encargado = formSubAct.encargado.completo;
    } else {
      this.subActividad.encargado = formSubAct.encargado;
    }
    this.listaSubAct.push(this.subActividad);

    this.subActividad = new SubActividad();
    this.formSubActividad.reset();
    this.agregar = false;
  }


  editarActividad(): void {
    this.formActividad.value.subActividad = this.listaSubAct;

    this.actividades.codObjetivo = this.formActividad.value.codObjetivo;
    this.actividades.actividad = this.formActividad.value.actividad;
    this.actividades.resultado = this.formActividad.value.resultado;
    this.actividades.estado = true;
    this.actividades.fechaIngresada = this.fechaIngresada;
    this.actividades.fechaActualizada = new Date();

    this.listaSubActEliminadas.forEach( s => {
      this.listaSubAct.push(s);
    });

    this.actividades.subActividades = this.listaSubAct;

    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));

    //console.log(this.actividades)
    this.service.actualizarActividad(this.actividades).subscribe(
      (response) => {
        Swal.fire({
              position: 'center',
              icon: 'success',
              title: `${response.mensaje}`,
              showConfirmButton: false,
              timer: 2000
            });

        this.router.navigate(['/actividad/catalogo']);
      }, (err) => {
        console.log(err.error);
        Swal.fire({
              position: 'center',
              icon: 'success',
              title: `${err.mensaje}`,
              showConfirmButton: false,
              timer: 2000
            });
      }
    );
  }

}
