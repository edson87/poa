package esam.edu.bo.poabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoaBackendApplication.class, args);
    }

}
