package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "objetivos")
public class Objetivo implements Serializable{
    private static long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "gestion")
    private String gestion;

    @Column(name = "area")
    private String area;

    private String sede;

    @Column(name = "codigoobjetivo")
    private String codigoobjetivo;

    @Column(name = "objetivogestion")
    private String objetivogestion;

    @Column(name = "productoresultadoobjetivo")
    private String productoresultadoobjetivo;

    @Column(name = "indicadorobjetivo")
    private String indicadorobjetivo;

    @Column(name = "medioverificacionobjetivo")
    private String medioverificacionobjetivo;

    @Column(name = "estado")
    private boolean estado;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fechahoraregistro")
    private Date fechahoraregistro;

    @Column(name = "codigousuario")
    private String codigousuario;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "objetivo")
    @JsonIgnoreProperties("objetivo")
    private List<CronogramaAnual> meses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "objetivo")
    @JsonIgnoreProperties("objetivo")
    private List<Actividad> actividades = new ArrayList<>();

    //@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "objetivo")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = "objetivo")
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGestion() {
        return gestion;
    }

    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getCodigoobjetivo() {
        return codigoobjetivo;
    }

    public void setCodigoobjetivo(String codigoobjetivo) {
        this.codigoobjetivo = codigoobjetivo;
    }

    public String getObjetivogestion() {
        return objetivogestion;
    }

    public void setObjetivogestion(String objetivogestion) {
        this.objetivogestion = objetivogestion;
    }

    public String getProductoresultadoobjetivo() {
        return productoresultadoobjetivo;
    }

    public void setProductoresultadoobjetivo(String productoresultadoobjetivo) {
        this.productoresultadoobjetivo = productoresultadoobjetivo;
    }

    public String getIndicadorobjetivo() {
        return indicadorobjetivo;
    }

    public void setIndicadorobjetivo(String indicadorobjetivo) {
        this.indicadorobjetivo = indicadorobjetivo;
    }

    public String getMedioverificacionobjetivo() {
        return medioverificacionobjetivo;
    }

    public void setMedioverificacionobjetivo(String medioverificacionobjetivo) {
        this.medioverificacionobjetivo = medioverificacionobjetivo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Date getFechahoraregistro() {
        return fechahoraregistro;
    }

    public void setFechahoraregistro(Date fechahoraregistro) {
        this.fechahoraregistro = fechahoraregistro;
    }

    public String getCodigousuario() {
        return codigousuario;
    }

    public void setCodigousuario(String codigousuario) {
        this.codigousuario = codigousuario;
    }

    public List<CronogramaAnual> getMeses() {
        return meses;
    }

    public void setMeses(List<CronogramaAnual> meses) {
        this.meses = meses;
    }

    public List<Actividad> getActividades() {
        return actividades;
    }

    public void setActividades(List<Actividad> actividades) {
        this.actividades = actividades;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
