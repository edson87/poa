package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Rol;

public interface IRolServices {
    public Rol saveRoles(Rol rol);
}
