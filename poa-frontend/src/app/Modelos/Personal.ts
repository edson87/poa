
export class Personal {
    id?: number;
    nombre: string;
    apellido: string;
    completo: string;
    area: string;
    sede: string;
}
