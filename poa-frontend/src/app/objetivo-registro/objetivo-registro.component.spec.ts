import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetivoRegistroComponent } from './objetivo-registro.component';

describe('ObjetivoRegistroComponent', () => {
  let component: ObjetivoRegistroComponent;
  let fixture: ComponentFixture<ObjetivoRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetivoRegistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetivoRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
