import { Component, OnInit, Input, DoCheck, Host, EventEmitter} from '@angular/core';
import { Token } from '../Modelos/token';
import { Router } from '@angular/router';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { ConfiguracionService } from '../configuracionService/configuracion.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, DoCheck {
  public gestion: any;
  public usuario: Token;
  public token: string;

  dato: any;
  constructor(private router: Router, private gestionService: ConfiguracionService) {
  }

  ngOnInit(): void {

    // Sede
    this.gestionService.currentmenuSede.subscribe(
      (mensaje) => {
        if (mensaje !== undefined) {
          this.usuario = mensaje;
          //console.log(this.usuario);
        }
      }
    );

    // desde objetivo.ts
    this.gestionService.currentmenu2.subscribe(
      (mensaje) => {
        if (mensaje !== undefined) {
          this.usuario = mensaje;
          //console.log(this.usuario);
        }
      }
    );

    // Para la actualizacion
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));

    // desde inicio.ts
    if (this.usuario === null) {
      this.gestionService.currentmenu.subscribe(
        (mensaje) => {
          this.usuario = mensaje;
          //console.log('desde inicio')
          //console.log(this.usuario);
        }
      );
    }
  }

  ngDoCheck(): void{
   //this.optenerToken();

  }

  optenerToken(): void {
    this.token = sessionStorage.getItem('token');
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    this.gestion = this.usuario.gestion;
  }

  salir(): void{
    sessionStorage.clear();
  }
}
