import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Token } from '../Modelos/token';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { Objetivo } from '../Modelos/Objetivo';
import { CronogramaAnual } from '../Modelos/CronogramaAnual';
import * as xlsx from 'xlsx';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { SubActividad } from '../Modelos/SubActividad';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-objetivo-catalogo',
  templateUrl: './objetivo-catalogo.component.html',
  styleUrls: ['./objetivo-catalogo.component.css']
})
export class ObjetivoCatalogoComponent implements OnInit {

  seleccionados: string;
  nuevoObjetivo: boolean = false;
  usuario: Token;
  public gestion: any;
  public listaGestiones = [];
  objetivos: Array<Objetivo>;
  gestionSeleccionada: string;
  sinRegistro: boolean = false;
  public meses: Array<string>;
  public arreglo = [];
  public registrar: boolean = false;
  public editarObjetivo: Objetivo;
  public listaVerObjetivo: Objetivo;
  public abilitarVer = false;
  public abilitarEditar = false;
  public listaCopiarObjetivo: Objetivo;
  public permisos: any = {};

  constructor(private router: ActivatedRoute,
              private service: ObjetivoServicioService,
              private route: Router) {
    this.usuario = new Token();
    this.objetivos = new Array<Objetivo>();
    this.meses = new Array<string>();
    this.listaVerObjetivo = new Objetivo();
   }

  ngOnInit(): void {
    this.obtenerGetionPorDefecto();
    this.obtenerTodalaGestion();
    this.obtenerPermisosUsuario();
    this.sinRegistro = true;
  }

  obtenerGetionPorDefecto(): void{
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  obtenerPermisosUsuario(): void {
    const permiso = sessionStorage.getItem('permisos');
    this.permisos = JSON.parse(atob(permiso));
  }

  abrirRegistro(): void {
    this.nuevoObjetivo = true;
  }

  public obtenerTodalaGestion(): any {
    const gestion = this.usuario.gestion;
    const area = this.usuario.area;
    const sede = this.usuario.sede;
    this.meses = new Array<string>();
    return this.service.obtenerObjetivoPorGestion(gestion, area, sede).subscribe(
      (response) => {
        this.objetivos = response ;
        console.log(this.objetivos)
        for (let i = 0; i < this.objetivos.length; i++) {
          const meses = this.objetivos[i].meses;
          for (let j = 0; j < meses.length; j++) {
            const mes = meses[j].mes;
            this.arreglo.push(mes);

          }

          const c = this.arreglo.toString();
          this.meses.push(c);
          this.arreglo = [];
        }

        this.sinRegistro = false;
      }, (err) => {
        console.log(err.error.mensaje);
        this.sinRegistro = true;
      }
    );
  }

  exportToExcel(objetivo): void{
    const title = 'POA GESTION ' + objetivo.gestion;
    const subTitle = 'Area: ' + this.usuario.area;
    const objTitle = objetivo.objetivogestion;

    const header = ['Codigo', 'Objetivo', 'Cronograma', 'Resultado',
                  'Indicadores Objetivo', 'Medios de Verificacion',
                  'Actividades', 'Sub-Actividad', 'Sub-Producto',
                  'Sub-Indicadores', 'Sub-Medios de Verificacion',
                  'Presupuesto', 'Responsable', 'Fecha Inicio', 'FechaFin'];


    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Objetivo');

    const titleRow = worksheet.addRow([title]);
    titleRow.font = {name: 'Arial', family: 4, size: 12, bold: true};

    const subTitleRow = worksheet.addRow([subTitle]);
    subTitleRow.font = {name: 'Arial', family: 4, size: 12, bold: true};

    const objetTitleRow = worksheet.addRow([objTitle]);
    objetTitleRow.font = {name: 'Arial', family: 4, size: 12, bold: true};

    worksheet.addRow([]);

    const headerRow = worksheet.addRow(header);
    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' }
      },
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });

    //console.log(objetivo)

    const objetivoObj = [];
    objetivoObj.push(objetivo);

    let actividadesObje = [];
    actividadesObje = objetivo.actividades;

    let activiCount = 0;
    let subActCount = [];

    activiCount = actividadesObje.length;

    for (let i = 0; i < actividadesObje.length; i++) {
      const element = actividadesObje[i];
      subActCount[i] = element.subActividades.length;
    }

    let totalSubAct = 0;
    subActCount.forEach(s => {
      totalSubAct += s;
    });

    let meses = '';
    const arreglo = [];
    for (let j = 0; j < objetivo.meses.length; j++) {
      const mes = objetivo.meses[j].mes;
      arreglo.push(mes);
    }
    const c = arreglo.toString();
    meses = c;
    //console.log(objetivo)
    let row;
    objetivoObj.forEach( o => {
      let lista = [objetivo.codigoobjetivo, objetivo.objetivogestion, meses, objetivo.productoresultadoobjetivo,
                      objetivo.indicadorobjetivo, objetivo.medioverificacionobjetivo, ];

      if (o.actividades.length === 0) {
          row = worksheet.addRow(lista);
      } else {
        o.actividades.forEach( a => {
            lista = [objetivo.codigoobjetivo, objetivo.objetivogestion, meses, objetivo.productoresultadoobjetivo,
                    objetivo.indicadorobjetivo, objetivo.medioverificacionobjetivo, a.actividad];

            const numSubAct = a.subActividades.length;
            let acceso = false;

            a.subActividades.forEach( s => {
              lista = [objetivo.codigoobjetivo, objetivo.objetivogestion, meses, objetivo.productoresultadoobjetivo,
              objetivo.indicadorobjetivo, objetivo.medioverificacionobjetivo, a.actividad, s.subActividad,
              s.resultadoObjetivo, s.subIndicador, s.subMediosVerificacion, s.presupuesto,
              s.encargado, s.fechaInicio, s.fechaFin];

              row = worksheet.addRow(lista);
            });
            if ( numSubAct > 1 && acceso === false) {
              worksheet.mergeCells(`G${row.number - (numSubAct - 1)}:G${row.number}`);
              acceso = true;
            }
          });
      }
    });

    if (objetivo.actividades.length !== 0) {
      worksheet.mergeCells(`A${6}:A${6 + (totalSubAct - 1)}`);
      worksheet.mergeCells(`B${6}:B${6 + (totalSubAct - 1)}`);
      worksheet.mergeCells(`C${6}:C${6 + (totalSubAct - 1)}`);
      worksheet.mergeCells(`D${6}:D${6 + (totalSubAct - 1)}`);
      worksheet.mergeCells(`E${6}:E${6 + (totalSubAct - 1)}`);
      worksheet.mergeCells(`F${6}:F${6 + (totalSubAct - 1)}`);
    }


    row.eachCell((cell, number) => {
        //cell.fill = {
        //type: 'pattern',
        //pattern: 'solid',
        //fgColor: { argb: 'FFFFFF00' },
        //bgColor: { argb: 'FF0000FF' }
      //},
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
    });

    worksheet.getColumn(2).width = 20;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 30;
    worksheet.getColumn(6).width = 30;
    worksheet.getColumn(7).width = 30;
    worksheet.getColumn(8).width = 20;
    worksheet.getColumn(9).width = 30;
    worksheet.getColumn(10).width = 30;
    worksheet.getColumn(11).width = 30;
    worksheet.getColumn(12).width = 15;
    worksheet.getColumn(13).width = 30;
    worksheet.getColumn(14).width = 20;
    worksheet.getColumn(15).width = 20;

    worksheet.addRow([]);

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Poa.xlsx');
    });

  }

  obtenerToken(): Token {
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  public registrarObjetivo(): void {
    this.service.abrirModal();
  }

  public abrirVentanaEditar(objetivo): void{
    this.route.navigate(['/objetivo/editar', objetivo.id]);
  }

  actualizar(event): void {
    if (event === true) {
      this.obtenerTodalaGestion();
    }
  }

  elimiarObjetivo(objetivo): void{
    Swal.fire({
      title: 'Esta seguro de querer eliminar el objetivo?',
      text: "",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar'
    }).then((result) => {

      if (result.isConfirmed) {
        this.service.eliminarObjetivos(objetivo).subscribe(
          (response) => {
            Swal.fire({
               position: 'center',
               icon: 'success',
               title: `${response.mensaje}`,
               showConfirmButton: false,
               timer: 2000
             });
            this.route.navigate(['/objetivo/catalogo']);
            this.ngOnInit();
          }, (err) => {
            console.log(err.error);
            Swal.fire({
              icon: 'error',
              title: '',
              text: `${err.mensaje}`,
              footer: ''
            });
          }
        );
      }

    })

  }

  verObjetivo(objetivo): void {
    this.listaVerObjetivo = objetivo;
    this.service.abrirModalVer();
    this.abilitarVer = true;
  }

  copiarObjetivo(objetivo): void {
    this.listaCopiarObjetivo = objetivo;
    this.service.abrirModelCopiar();
    this.abilitarEditar = true;
  }

  copiarTodosObjetivos(objetivos) {
    this.listaCopiarObjetivo = objetivos;
    this.service.abrirModelCopiar();
    this.abilitarEditar = true;
  }
}
