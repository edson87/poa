package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Actividad;
import esam.edu.bo.poabackend.web.models.SubActividad;

import java.util.List;

public interface ISubActividadService {
    public SubActividad saveSubActividad(SubActividad subActividades);

    public void deleteSubactividadByActividad(Actividad actividad);

    public List<SubActividad> findAllSubActividadesInTrue(Actividad actividad);
}
