package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.ICronogramaDao;
import esam.edu.bo.poabackend.web.models.CronogramaAnual;
import esam.edu.bo.poabackend.web.models.Objetivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CronogramaServiceImpl implements ICronogramaService {

    @Autowired
    ICronogramaDao cronogramaDao;

    @Override
    public CronogramaAnual saveInObjetive(CronogramaAnual mes) {
        return cronogramaDao.save(mes);
    }

    @Override
    @Transactional(readOnly = true)
    public void delateCronogramasById(Objetivo objetivo) {
        cronogramaDao.deleteCronogramaAnualByObjetivo(objetivo);
    }
}
