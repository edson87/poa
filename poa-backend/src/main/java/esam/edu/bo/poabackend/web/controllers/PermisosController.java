package esam.edu.bo.poabackend.web.controllers;

import esam.edu.bo.poabackend.services.ObjetivoSeviceImpl;
import esam.edu.bo.poabackend.services.PermisoImplServ;
import esam.edu.bo.poabackend.services.UsuarioImplServ;
import esam.edu.bo.poabackend.web.models.Objetivo;
import esam.edu.bo.poabackend.web.models.Permiso;
import esam.edu.bo.poabackend.web.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin
@RequestMapping("/api/permisos")
@RestController
public class PermisosController {

    @Autowired
    PermisoImplServ permisoImpl;

    @Autowired
    ObjetivoSeviceImpl objetivoSevice;

    @Autowired
    UsuarioImplServ usuarioImpl;

    @GetMapping()
    public ResponseEntity<?> getAllPermisos() {
        Map<String, Object> message = new HashMap<>();
        List<Permiso> permisos = permisoImpl.getAllPermisos();
        List<Permiso> lista = new ArrayList<>();
        boolean iguales = false;

        try {
            inicial:
            for (int i = 0; i < permisos.size(); i++) {
                Permiso p = permisos.get(i);

                if (lista.size() == 0) {
                    lista.add(p);
                } else {

                    for (int j = 0; j < lista.size(); j++) {
                        Permiso l = lista.get(j);

                        if (!p.getRol().equals(l.getRol())) {
                            iguales = true;
                        } else {
                            iguales = false;
                            continue inicial;
                        }

                    }

                    if (iguales == true) {
                        lista.add(p);
                    }

                }

            }
        } catch (Exception e) {
            message.put("mensaje", "Error al devolver los permisos");
            message.put("error", e.getCause().getMessage());
        }

        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PutMapping("/rol")
    public ResponseEntity<?> updatePermisosByRol(@RequestBody Permiso permiso) {
        Map<String, Object> message = new HashMap<>();
        Permiso nuevoPermiso = new Permiso();
        List<Permiso> permisos = new ArrayList<>();

        try {

            permisos = permisoImpl.getPermisosByRol(permiso.getRol());

            for (Permiso p: permisos) {
                p.setLeer_objetivo(permiso.isLeer_objetivo());
                p.setCrear_objetivo(permiso.isCrear_objetivo());
                p.setEditar_objetivo(permiso.isEditar_objetivo());
                p.setEliminar_objetivo(permiso.isEliminar_objetivo());
                p.setExcel_objetivo(permiso.isExcel_objetivo());
                p.setCopiar_objetivo(permiso.isCopiar_objetivo());

                p.setLeer_actividad(permiso.isLeer_actividad());
                p.setCrear_actividad(permiso.isCrear_actividad());
                p.setEditar_actividad(permiso.isEditar_actividad());
                p.setEliminar_actividad(permiso.isEliminar_actividad());

                permisoImpl.updatePermisoByRol(p);
            }

        } catch (DataAccessException e) {
            message.put("mensaje", "No se pudo actualizar lo permisos");
            message.put("error", e.getCause());
        }

        message.put("mensaje","Se editaron los permisos");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/usuario/{codUsuario}")
    public ResponseEntity<?> getPermisosByCodUsuario(@PathVariable("codUsuario") String codUsuario) {
        Permiso permisos = new Permiso();
        Usuario usuario = new Usuario();
        Map<String, Object> message = new HashMap<>();

        try {
            permisos = permisoImpl.getPermisos(codUsuario);

        }catch (DataAccessException e) {
            message.put("mensaje", "no se pudo encontrar los permisos!");
            message.put("error", e.getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(permisos, HttpStatus.OK);
    }

    /*@PostMapping("/usuario/{codUsuario}")
    public ResponseEntity<?> insertPermiso(@RequestBody Permiso permiso, @PathVariable("codUsuario") String codUsuario) {
        Objetivo objetivo = new Objetivo();
        Usuario usuario = new Usuario();
        Usuario newUsuario = new Usuario();
        Map<String, Object> message = new HashMap<>();

        try {
            //newUsuario.setCod_usuario(codUsuario);
            //newUsuario.setPermiso(permiso);
            //usuario = usuarioImpl.insertUsuario(newUsuario);

            System.out.println(usuario);
            //objetivo = objetivoSevice.getObjetivoByCodUsuario(codUsuario);

            //permiso.setObjetivo(objetivo);

            //permisoImpl.savePermisos(permiso);

           // objetivo.setPermisos(permiso);
           // objetivoSevice.saveObjetivos(objetivo);

        } catch (DataAccessException e) {
            message.put("mensaje", "no se pudo insertar los permisos!");
            message.put("error", e.getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "permiso insertado correctamente.");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }*/
}
