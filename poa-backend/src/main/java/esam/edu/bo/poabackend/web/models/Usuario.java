package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {
    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;

    private String codigousuario;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario")
    @JsonIgnoreProperties(value = "objetivo")
    private List<Objetivo> objetivo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario")
    @JsonIgnoreProperties(value = "usuario")
    /*@OneToMany()
    @JoinTable(name = "user_permiso",
            joinColumns = {@JoinColumn(name = "user_fk", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "permiso_fk", referencedColumnName = "id")}
    )*/
    private Permiso permisos;

    /*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario")
    @JsonIgnoreProperties(value = "roles")
    private List<Rol> roles;*/
}
