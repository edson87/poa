import { Component, OnInit } from '@angular/core';
import { ConfiguracionService } from '../../configuracionService/configuracion.service';
import { Token } from '../../Modelos/token';
import { ObjetivoServicioService } from '../../objetivo-servicios/objetivo-servicio.service';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.css']
})
export class GestionComponent implements OnInit {
  gestionSeleccionada: string;
  public gestion: any;
  public usuario: Token;
  public listaGestiones = [];

  constructor(private service: ConfiguracionService, private serviceObj: ObjetivoServicioService) { }

  ngOnInit(): void {
    this.mostrarGestiones();
  }

  obtenerToken(): Token {
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  editarToken(usuario: Token): void {
    sessionStorage.removeItem('usuario');
    sessionStorage.setItem('usuario', JSON.stringify(usuario));
  }

  mostrarGestiones(): void{
    const area = this.obtenerToken().area;

    this.serviceObj.obtenerLasGestiones(area).subscribe(
      (response) => {
        this.listaGestiones = response;
        const añoActual = new Date().getFullYear();
     
        if (this.listaGestiones.includes(añoActual.toString()) === false){
          this.listaGestiones.push(añoActual);

          // Selecciona la gestion por defecto y busca la gestion en la DB
          this.gestionSeleccionada = añoActual.toString();
          const gestion = this.obtenerToken().area;
        }else{
          // Selecciona la gestion por defecto y busca la gestion en la DB
          this.gestionSeleccionada = '';
          const gestion = this.obtenerToken().area;
        }

      }
    );
  }

  click(seleccionados): void {
    this.gestion = seleccionados;
    // obtengo el token
    this.usuario = this.obtenerToken();
    // ingreso el token a una variable para pasarla al menu
    this.usuario.gestion = this.gestion;
    this.mandarToken(this.usuario);
    this.editarToken(this.usuario);
  }

  public mandarToken(token: Token): void {
    this.service.mandarMensajeDesdeObjetivo(token);
  }
}
