import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Token } from '../Modelos/token';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { ConfiguracionService } from '../configuracionService/configuracion.service';
import { PermisosServiciosService } from '../permisos-servicios/permisos-servicios.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})

export class InicioComponent implements OnInit {

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkNhcmxvcyBNb250ZWxsYW5vcyIsImdlc3Rpb24iOiIyMDIwIiwiYXJlYSI6IlNpc3RlbWFzIiwic2VkZSI6IkVzYW0gVGFyaWphIENlbnRyYWwiLCJyb2wiOiJhZG1pbiIsInVzdWFyaW8iOiJjMTIiLCJpYXQiOjE1MTYyMzkwMjJ9.K47gC1v8IDcDiiOEdmSkotIwKyr4xsiMpH7IYJ6eEm8
  public tokenUrlSisAdm: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkphaW1lIEhpbm9qb3phIiwiZ2VzdGlvbiI6IjIwMjAiLCJhcmVhIjoiU2lzdGVtYXMiLCJzZWRlIjoiRXNhbSBUYXJpamEgQ2VudHJhbCIsInJvbCI6ImFkbWluIiwidXN1YXJpbyI6ImMxMiIsImlhdCI6MTUxNjIzOTAyMn0.wVHkiSs40KM7QqlJgZ6IS8zk6vTx6-rGNAu6A_2G95A';
  public tokenUrlAdmAdm: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkNhcmxvcyBNb250ZWxsYW5vIiwiZ2VzdGlvbiI6IjIwMjAiLCJhcmVhIjoiQWRtaW5pc3RyYWNpb24iLCJzZWRlIjoiQ2VudHJhbCIsInJvbCI6ImFkbWluIiwidXN1YXJpbyI6ImM1NSIsImlhdCI6MTUxNjIzOTAyMn0.fMMmFK8B1PB7TWCtqQlOxdZ_hAaNe7rf1mef26qlV9o';
  public tokenUrlSis: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1hdXJpY2lvIFplZ2FycmEiLCJnZXN0aW9uIjoiMjAxOSIsImFyZWEiOiJTaXN0ZW1hcyIsInNlZGUiOiJFc2FtIENvY2hhYmFtYmEiLCJyb2wiOiJ1c2VyIiwidXN1YXJpbyI6ImMxMCIsImlhdCI6MTUxNjIzOTAyMn0.0uNUrGHq1PVfgs2VeKLmKsBa2fL4HJCXJi0jxgp9wvs';
  public token: Token;
  public permisos: any = { leer: false, escribir: false, eliminar: false, todos: false };

  constructor(private route: ActivatedRoute, private service: ConfiguracionService,
              private permisosServicio: PermisosServiciosService) {
    this.token = new Token();
  }

  ngOnInit(): void {
    const otro = JSON.parse(sessionStorage.getItem('usuario'));

    if (otro === null) {
      this.optenerId();
    } else {
      sessionStorage.removeItem('usuario');
      this.token = otro;
      sessionStorage.setItem('usuario', JSON.stringify(this.token));
      this.mandarToken(this.token);
    }
    this.permisosPorDefecto();
  }

  optenerId(): void {
    //sessionStorage.clear();
    this.route.params.subscribe((params) => {
      const id = params['id']
      sessionStorage.setItem('token', id);
      if (id) {
        const variable = JSON.parse(atob(id.split('.')[1]));

        this.token.name = variable.name;
        this.token.area = variable.area;
        this.token.sede = variable.sede;
        this.token.gestion = variable.gestion;
        this.token.rol = variable.rol;
        this.token.usuario = variable.usuario;

        sessionStorage.setItem('usuario', JSON.stringify(this.token));
        this.mandarToken(this.token);
      }
    });
  }

  public mandarToken(token: Token): void {
    this.service.mandarMensaje(token);
  }

  permisosPorDefecto(): void {
    if (this.token) {
      const cod_usuario = this.token.usuario;
      console.log(this.token)
      this.permisosServicio.obtenerPermisosPorUsuario(cod_usuario).subscribe(
        (response) => {
          const permisos = response;
          //console.log(permisos)
          if (permisos === null) {
            console.log("no tienen permisos")
            if (this.token.rol === 'admin') {
              this.permisos.rol = 'admin';

              this.permisos.leer_objetivo = true;
              this.permisos.crear_objetivo = true;
              this.permisos.editar_objetivo = true;
              this.permisos.eliminar_objetivo = true;
              this.permisos.excel_objetivo = true;
              this.permisos.copiar_objetivo = true;

              this.permisos.leer_actividad = true;
              this.permisos.crear_actividad = true;
              this.permisos.editar_actividad = true;
              this.permisos.eliminar_actividad = true;

              sessionStorage.setItem('permisos', btoa(JSON.stringify(this.permisos)));

            } else if (this.token.rol === 'user') {
              this.permisos.rol = 'user';

              this.permisos.leer_objetivo = true;
              this.permisos.crear_objetivo = true;
              this.permisos.editar_objetivo = true;
              this.permisos.eliminar_objetivo = false;
              this.permisos.excel_objetivo = false;
              this.permisos.copiar_objetivo = false;

              this.permisos.leer_actividad = true;
              this.permisos.crear_actividad = true;
              this.permisos.editar_actividad = true;
              this.permisos.eliminar_actividad = false;

              sessionStorage.setItem('permisos', btoa(JSON.stringify(this.permisos)));
            } else if (this.token.rol === 'jefe') {

              this.permisos.rol = 'jefe';

              this.permisos.leer_objetivo = true;
              this.permisos.crear_objetivo = true;
              this.permisos.editar_objetivo = true;
              this.permisos.eliminar_objetivo = true;
              this.permisos.excel_objetivo = true;
              this.permisos.copiar_objetivo = true;

              this.permisos.leer_actividad = true;
              this.permisos.crear_actividad = true;
              this.permisos.editar_actividad = true;
              this.permisos.eliminar_actividad = true;

              sessionStorage.setItem('permisos', btoa(JSON.stringify(this.permisos)));
            } else if (this.token.rol === 'encargado') {

              this.permisos.rol = 'encargado';

              this.permisos.leer_objetivo = true;
              this.permisos.crear_objetivo = true;
              this.permisos.editar_objetivo = true;
              this.permisos.eliminar_objetivo = true;
              this.permisos.excel_objetivo = true;
              this.permisos.copiar_objetivo = false;

              this.permisos.leer_actividad = true;
              this.permisos.crear_actividad = true;
              this.permisos.editar_actividad = true;
              this.permisos.eliminar_actividad = true;

              sessionStorage.setItem('permisos', btoa(JSON.stringify(this.permisos)));
            }

          }else {
            console.log("ya tiene permisos")
            this.permisos = permisos;
            sessionStorage.setItem('permisos', btoa(JSON.stringify(this.permisos)));
          }
        }, (err) => {
          console.log(err);
        }
      );
    }

  }

  insertarPermisos(permisos: any, cod_usuario: string): void {
    this.permisosServicio.insertarPermisoPorDefectoAUsuario(this.permisos, cod_usuario).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }

}
