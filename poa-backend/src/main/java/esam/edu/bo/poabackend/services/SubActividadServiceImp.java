package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.ISubActividadDao;
import esam.edu.bo.poabackend.web.models.Actividad;
import esam.edu.bo.poabackend.web.models.SubActividad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SubActividadServiceImp implements ISubActividadService {

    @Autowired
    ISubActividadDao subActividadDao;

    @Override
    public SubActividad saveSubActividad(SubActividad subActividades) {

        return subActividadDao.save(subActividades);
    }

    @Override
    @Transactional(readOnly = true)
    public void deleteSubactividadByActividad(Actividad actividad) {
        subActividadDao.deleteSubActividadByActividad(actividad);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubActividad> findAllSubActividadesInTrue(Actividad actividad) {
        return subActividadDao.findSubActividadesByActividad(actividad);
    }

}
