import { Component, OnInit, Input } from '@angular/core';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Objetivo } from '../Modelos/Objetivo';
import { CronogramaAnual } from '../Modelos/CronogramaAnual';
import { Router, ActivatedRoute } from '@angular/router';
import  Swal  from 'sweetalert2';


@Component({
  selector: 'app-objetivo-editar',
  templateUrl: './objetivo-editar.component.html',
  styleUrls: ['./objetivo-editar.component.css']
})
export class ObjetivoEditarComponent implements OnInit {
  @Input() editarObjetivo: Objetivo;
  formObjetivo: FormGroup;
  formCronograma: FormGroup;
  objetivo: Objetivo;
  cronograma: CronogramaAnual;
  listaCronograma: Array<CronogramaAnual> = [];

  constructor(public servicio: ObjetivoServicioService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.objetivo = new Objetivo();
    this.listaCronograma = new Array<CronogramaAnual>();
    this.cronograma = new CronogramaAnual();
    this.inicializarFormularioObjetivo();
    this.inicializarCronogramaMesual();

    this.route.params.subscribe((params) => {
      let id = params['id'];

      if (id !== null) {
        this.servicio.obtenerObjetivoPorId(id).subscribe(
          (response) => {
            if (response) {
              this.objetivo = response;
              this.inicializarFormularioObjetivo();
              const listaMeses: any = this.objetivo.meses;

              for (let i = 0; i < listaMeses.length; i++) {
                const element = listaMeses[i].mes;
                this.editarMesas(element);
              }
              this.inicializarCronogramaMesual();
            }
          }
        );
      } else {
        this.router.navigate(['/objetivo/catalogo']);
      }
    });
  }

  public inicializarFormularioObjetivo(): void {
    this.formObjetivo = new FormGroup({
      codigoobjetivo: new FormControl(this.objetivo.codigoobjetivo, {
        validators: [Validators.required]
      }),
      objetivogestion: new FormControl(this.objetivo.objetivogestion, {
        validators: [Validators.required]
      }),
      productoresultadoobjetivo: new FormControl(this.objetivo.productoresultadoobjetivo, {
        validators: [Validators.required]
      }),
      meses: new FormControl('', {
        validators: [Validators.required]
      }),
      indicadorobjetivo: new FormControl(this.objetivo.indicadorobjetivo, {
        validators: [Validators.required]
      }),
      medioverificacionobjetivo: new FormControl(this.objetivo.medioverificacionobjetivo, {
        validators: [Validators.required]
      })
    });
  }

  inicializarCronogramaMesual(): void {
    this.formCronograma = new FormGroup({
      enero: new FormControl(this.listaCronograma['enero'], {
        validators: []
      }),
      febrero: new FormControl(this.listaCronograma['febrero'], {
        validators: []
      }),
      marzo: new FormControl(this.listaCronograma['marzo'], {
        validators: []
      }),
      abril: new FormControl(this.listaCronograma['abril'], {
        validators: []
      }),
      mayo: new FormControl(this.listaCronograma['mayo'], {
        validators: []
      }),
      junio: new FormControl(this.listaCronograma['junio'], {
        validators: []
      }),
      julio: new FormControl(this.listaCronograma['julio'], {
        validators: []
      }),
      agosto: new FormControl(this.listaCronograma['agosto'], {
        validators: []
      }),
      septiembre: new FormControl(this.listaCronograma['septiembre'], {
        validators: []
      }),
      octubre: new FormControl(this.listaCronograma['octubre'], {
        validators: []
      }),
      noviembre: new FormControl(this.listaCronograma['noviembre'], {
        validators: []
      }),
      diciembre: new FormControl(this.listaCronograma['diciembre'], {
        validators: []
      })
    });
  }

  public editarMesas(mes: string): void {
    const lista: any = {};

    if (mes === 'Enero') {
      this.listaCronograma['enero'] = true;

    } else if (mes === 'Febrero') {
      this.listaCronograma['febrero'] = true;

    } else if (mes === 'Marzo') {
      this.listaCronograma['marzo'] = true;

    } else if (mes === 'Abril') {
      this.listaCronograma['abril'] = true;

    } else if (mes === 'Mayo') {
      this.listaCronograma['mayo'] = true;

    } else if (mes === 'Junio') {
      this.listaCronograma['junio'] = true;

    } else if (mes === 'Julio') {
      this.listaCronograma['julio'] = true;

    } else if (mes === 'Agosto') {
      this.listaCronograma['agosto'] = true;

    } else if (mes === 'Septiembre') {
      this.listaCronograma['septiembre'] = true;

    } else if (mes === 'Octubre') {
      this.listaCronograma['octubre'] = true;

    } else if (mes === 'Noviembre') {
      this.listaCronograma['noviembre'] = true;

    } else if (mes === 'Diciembre') {
      this.listaCronograma['diciembre'] = true;

    }
  }

  public mesesLiteral(numero: number): string{
    let mes: any = '';
    if (numero === 0) {
      mes = 'Enero';
    } else if (numero === 1) {
      mes = 'Febrero';
    } else if (numero === 2) {
      mes = 'Marzo';
    } else if (numero === 3) {
      mes = 'Abril';
    } else if (numero === 4) {
      mes = 'Mayo';
    } else if (numero === 5) {
      mes = 'Junio';
    } else if (numero === 6) {
      mes = 'Julio';
    } else if (numero === 7) {
      mes = 'Agosto';
    } else if (numero === 8) {
      mes = 'Septiembre';
    } else if (numero === 9) {
      mes = 'Octubre';
    } else if (numero === 10) {
      mes = 'noviembre';
    } else if (numero === 11) {
      mes = 'Diciembre';
    }
    return mes;
  }

  guardarObjetivo(): void {
    const meses = Object.values(this.formCronograma.value);
    this.listaCronograma = new Array<CronogramaAnual>();

    for (let index = 0; index < meses.length; index++) {
      const element = meses[index];

      if (element === true){
        this.cronograma.mes = this.mesesLiteral(index);
        this.listaCronograma.push(this.cronograma);
        this.cronograma = new CronogramaAnual();
      }
    }
    console.log(this.listaCronograma)
    // this.usuario = JSON.parse(sessionStorage.getItem('usuario'));

    this.objetivo.codigoobjetivo = this.formObjetivo.value.codigoobjetivo;
    this.objetivo.objetivogestion = this.formObjetivo.value.objetivogestion;
    this.objetivo.gestion = this.objetivo.gestion;
    this.objetivo.area = this.objetivo.area;
    this.objetivo.productoresultadoobjetivo = this.formObjetivo.value.productoresultadoobjetivo;
    this.objetivo.meses = this.listaCronograma;
    this.objetivo.indicadorobjetivo = this.formObjetivo.value.indicadorobjetivo;
    this.objetivo.medioverificacionobjetivo = this.formObjetivo.value.medioverificacionobjetivo;
    this.objetivo.estado = 1;
    this.objetivo.fechahoraregistro = new Date();
    this.objetivo.codigousuario = '';
    console.log(this.objetivo);

    this.servicio.actualizarObjetivos(this.objetivo).subscribe(
      (response) => {

        Swal.fire({
            position: 'center',
            icon: 'success',
            title: `${response.mensaje}`,
            showConfirmButton: false,
            timer: 2000
          });
        this.router.navigate(['/objetivo/catalogo']);
      }, (err) => {
        console.log(err.error);
        Swal.fire({
          icon: 'error',
          title: '',
          text: `${err.mensaje}`,
          footer: ''
        })
      }
    );
  }

}
