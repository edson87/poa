package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.CronogramaAnual;
import esam.edu.bo.poabackend.web.models.Objetivo;

public interface ICronogramaService {
    public CronogramaAnual saveInObjetive(CronogramaAnual mes);

    public void delateCronogramasById(Objetivo objetivo);
}
