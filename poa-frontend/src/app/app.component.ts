import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Token } from './Modelos/token';
import { ActivatedRoute } from '@angular/router';
import { ObjetivoServicioService } from './objetivo-servicios/objetivo-servicio.service';
import { InicioComponent } from './inicio/inicio.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges {
  listaGestiones = [];
  title = 'poa-frondend';
  token: Token;
  promiseGreeting: any;


  constructor(private route: ActivatedRoute, private service: ObjetivoServicioService) {
    this.token = new Token();
  }

  ngOnInit(): void{
    //this.mostrarGestiones();
  }

  ngOnChanges(): void{
    //this.asyncFunction().then(result => console.log(result));
  }

  obtenerToken(): void{
    this.promiseGreeting = () => new Promise(((resolve) => {
      this.token = JSON.parse(sessionStorage.getItem('usuario'));
      resolve(this.token.name);
    }));
  }
  /*asyncFunction = async () => {
    this.promiseGreeting = () => new Promise(((resolve) => {
      this.token = JSON.parse(sessionStorage.getItem('usuario'));
      if (this.token !== undefined || this.token !== null) {
        resolve(this.token.name);
      }
    }));
  }*/
  /*mostrarGestiones(): void{
    this.service.obtenerLasGestiones().subscribe(
      (response) => {
        this.listaGestiones = response;
      }
    );
  }*/

}
