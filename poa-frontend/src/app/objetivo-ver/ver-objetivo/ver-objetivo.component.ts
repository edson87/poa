import { Component, OnInit, Input } from '@angular/core';
import { ObjetivoServicioService } from '../../objetivo-servicios/objetivo-servicio.service';
import { Objetivo } from 'src/app/Modelos/Objetivo';

@Component({
  selector: 'app-ver-objetivo',
  templateUrl: './ver-objetivo.component.html',
  styleUrls: ['./ver-objetivo.component.css']
})
export class VerObjetivoComponent implements OnInit {

  @Input() listaVerObjetivo: Objetivo;
  arreglo = [];
  meses: string;

  constructor(public servicio: ObjetivoServicioService) { }

  ngOnInit(): void {
     console.log('objetivo',this.listaVerObjetivo)
    this.unificarMeses();
  }

  unificarMeses(): void {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.listaVerObjetivo.meses.length; i++) {
      const mes = this.listaVerObjetivo.meses[i].mes;
      this.arreglo.push(mes);
    }
    const c = this.arreglo.toString();
    this.meses = c;
    this.arreglo = [];
  }

  cerrarModal(): void {
    this.servicio.cerrarModalVer();
  }
}
