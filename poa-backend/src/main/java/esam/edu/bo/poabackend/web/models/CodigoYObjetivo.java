package esam.edu.bo.poabackend.web.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodigoYObjetivo {
    private String codigo;
    private String objetivo;

}
