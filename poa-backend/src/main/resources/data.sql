

insert into personal(nombre, apellidos, area) values('Edson','Lopez Cortez', 'Sistemas');

insert into objetivos(area, codigoobjetivo, codigousuario, estado, fechahoraregistro, gestion, indicadorobjetivo, medioverificacionobjetivo, objetivogestion, productoresultadoobjetivo)
values('Sistemas', 'CODS-1', 'U-1', true, '2020-07-21 21:16:45.518000', '2019', 'xxx', 'xxx', 'xxx', 'xxx'),
('Contabilidad', 'CODS-2', 'U-1', true, '2020-07-21 21:16:45.518000', '2018', 'ddd', 'ddd', 'ddd', 'ddd'),
('Marketing', 'CODS-3', 'U-1', true, '2020-07-21 21:16:45.518000', '2017', 'yyy', 'yyy', 'yyy', 'yyy');

insert into cronogramas(mes, objetivo_id) values('Enero',1), ('Febrero',1), ('Marzo',1),('Enero',2);


insert into actividades(actividad, cod, estado, ingreso, actualizado, objetivo_id)
values('actividad pricipal', 'CODS-1', true, '2020-07-21 21:16:45.518000', '2020-07-21 21:16:45.518000', 1);


insert into subactividades(inicio,fin,actividad,resultado,indicador,medios_verificacion,presupuesto,encargado,actividad_id)
values('2020-07-1','2020-07-21','sub actividad','resultado','indicador','medios de verificacioin',33.5,'encargado',1);