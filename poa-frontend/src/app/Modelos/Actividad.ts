import { SubActividad } from './SubActividad';

export class Actividad {
    id?: number;
    actividad: string;
    resultado: string;
    codObjetivo?: string;
    estado: true;
    fechaIngresada?: Date;
    fechaActualizada: Date;
    subActividades: Array<SubActividad>;
}