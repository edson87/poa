package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.IObjetivosDao;
import esam.edu.bo.poabackend.web.models.Objetivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ObjetivoSeviceImpl implements IObjetivoService{


    @Autowired
    IObjetivosDao iObjetivosDao;

    @Override
    //@Transactional(readOnly = true)
    public List<Objetivo> getAllObjetivos() {
        return iObjetivosDao.findAll();
    }

    @Override
    public Objetivo saveObjetivos(Objetivo objetivos) {
        return iObjetivosDao.save(objetivos);
    }

    @Override
    public Objetivo getObjetivos(long id, boolean estado) {
        return iObjetivosDao.queryFindByIdAndEstado(id, estado);
    }

    @Override
    public Objetivo updateObjetivos(Objetivo objetivos) {
        return iObjetivosDao.save(objetivos);
    }

    @Override
    public void deleteObjetivos(long id) {
        iObjetivosDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getAllManagements(String area, boolean estado) {
        List<String> managements = iObjetivosDao.findAllManagements(area, estado);
        return managements;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Objetivo> getObjetiveByManagementAndAreaAndSede(String gestion, String area, boolean estado, String sede) {
        List<Objetivo> objetivo = iObjetivosDao.findObjetivoByGestionAndAreas(gestion, area, estado, sede);
        return objetivo;
    }

    @Override
    @Transactional(readOnly = true)
    public Objetivo getObjetiveByCodObjeAndAreaAndSede(String codObje, String area, boolean estado, String sede) {
        Objetivo objetivo = iObjetivosDao.findObjetivoByCodObjeAndAreaAndSede(codObje, area, estado, sede);
        return objetivo;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object[]> getAllCodObjetivos(String area, String gestion, boolean estado, String sede) {
        List<Object[]> codigos = iObjetivosDao.findAllCodObjetivo(area, gestion, estado, sede);
        return codigos;
    }

    @Override
    @Transactional(readOnly = true)
    public Objetivo getObjetivoByCodUsuario(String cod_usuario) {
        Objetivo objetivo = iObjetivosDao.findByCodigousuario(cod_usuario);
        return objetivo;
    }
}
