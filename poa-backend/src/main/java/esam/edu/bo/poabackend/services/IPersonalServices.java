package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Personal;

import java.util.List;

public interface IPersonalServices {
    public List<Personal> getAllPersonals();
}
