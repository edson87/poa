import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="error">
      <h1 >404</h1>
      <h1 >This Page Not Found</h1>
    </div>
  `,
  styles: [
    '.error {margin-top: 5%;}',
    'h1 {color: #3fa9f5; text-align: center;}'
  ]
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
