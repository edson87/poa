package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonalDao extends JpaRepository<Personal, Long> {
}
