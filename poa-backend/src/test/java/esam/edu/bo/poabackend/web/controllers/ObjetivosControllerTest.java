package esam.edu.bo.poabackend.web.controllers;

import static org.junit.Assert.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import esam.edu.bo.poabackend.services.*;
import esam.edu.bo.poabackend.web.models.Objetivo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ObjetivosController.class)
public class ObjetivosControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ObjetivoSeviceImpl objetivoSevice;

    @MockBean
    CronogramaServiceImpl cronogramaService;

    @MockBean
    ActividadServicesImp actividadServices;

    @MockBean
    PermisoImplServ permisoImpl;

    @MockBean
    UsuarioImplServ usuarioImpl;

    @MockBean
    SubActividadServiceImp subActividadService;

    @Test
    public void getAllObjetivos() throws Exception {
        mockMvc.perform(get("/api/objetivos/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getObjetivos() throws  Exception {
        /*Long id = Long.valueOf(1);
        boolean estado = true;

        this.mockMvc.perform(get("/api/objetivos/"+id+"/1")).andDo(print())
                .andExpect(status().isOk());*/

    }

    @Test
    public void getObjetivo() throws  Exception {
        Integer id = 1;
        boolean estado = true;

        this.mockMvc.perform(get("/api/objetivos/2020/Sistemas/"+true+"/Esam Cochabamba")).andDo(print())
                .andExpect(status().isOk());

    }
}