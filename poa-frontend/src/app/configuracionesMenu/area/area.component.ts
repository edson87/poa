import { Component, OnInit } from '@angular/core';
import { ConfiguracionService } from '../../configuracionService/configuracion.service';
import { Token } from '../../Modelos/token';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {
  public listaAreas: any = ['Sistemas', 'Administracion'];
  public areaSeleccionada: any
  public usuario: Token;

  constructor(private service: ConfiguracionService) { }

  ngOnInit(): void {
    this.areaSeleccionada = '';
    this.usuario = new Token();
  }

  obtenerToken(): Token {
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  editarToken(usuario: Token): void {
    sessionStorage.removeItem('usuario');
    sessionStorage.setItem('usuario', JSON.stringify(usuario));
  }

  click(areaSeleccionada) {
    console.log(areaSeleccionada)
    this.usuario = this.obtenerToken();
    this.usuario.area = areaSeleccionada;
    this.editarToken(this.usuario);
    this.mandarToken(this.usuario);
  }

  public mandarToken(token: Token): void {
    this.service.mandarMensajeDesdeObjetivo(token);
  }
}
