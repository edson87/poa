package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
//@Configuration
//@EnableJpaAuditing
@Entity
@Table(name = "actividades")
public class Actividad implements Serializable {
    private static long serialVersionUID = 2998355717598169235L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String actividad;

    private String resultado;

    @NotNull
    @Column(name = "cod")
    private String codObjetivo;

    private boolean estado;

    //@CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ingreso", nullable = false, updatable = false)
    private Date fechaIngresada;

    //@LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "actualizado", insertable = true)
    private Date fechaActualizada;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "actividad")
    @JsonIgnoreProperties("actividad")
    private List<SubActividad> subActividades = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "objetivo_id")
    @JsonIgnoreProperties("objetivo")
    private Objetivo objetivo;

}
