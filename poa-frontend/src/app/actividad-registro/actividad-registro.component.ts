import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { Actividad } from '../Modelos/Actividad';
import { SubActividad } from '../Modelos/SubActividad';
import { ActividadService } from '../actividad-service/actividad.service';
import { Token } from '../Modelos/token';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Personal } from '../Modelos/Personal';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-actividad-registro',
  templateUrl: './actividad-registro.component.html',
  styleUrls: ['./actividad-registro.component.css']
})
export class ActividadRegistroComponent implements OnInit {

  public actividades: Actividad;
  public subActividad: SubActividad;
  public listaSubAct: Array<SubActividad> = [];
  public formActividad: FormGroup;
  public formSubActividad: FormGroup;
  public agregar: boolean = false;
  public index = 0;
  public usuario: Token;

  public listaCodigos: Array<any>;
  public codigoSeleccionada: string;


  public keyword = 'completo';
  public data$: Observable<any>;

  constructor(private actividadServ: ActividadService, public service: ActividadService,
              private router: Router) {
    this.actividades = new Actividad();
    this.subActividad = new SubActividad();
    this.listaSubAct = new Array<SubActividad>();
    this.usuario = new Token();
    this.listaCodigos = [];
  }

  ngOnInit(): void {
    this.inicializarActividad();
    this.obtenerCodigosObjetivos();
  }

  inicializarActividad(): void {
    this.formActividad = new FormGroup({
      codObjetivo: new FormControl('', {
        validators: []
      }),
      actividad: new FormControl('', {
        validators: [Validators.required]
      }),
      resultado: new FormControl('', {
        validators: []
      })
    });
  }

  public agregarSubActividad(): void{
    if (this.agregar === false) {
      this.agregar = true;
      this.iniciarSubActividad();
      this.obtenerListaPersonales();
    }
  }

  public iniciarSubActividad(): void {
    this.formSubActividad = new FormGroup({
      fechaInicio: new FormControl('', {
        validators: [Validators.required]
      }),
      fechaFin: new FormControl('', {
        validators: [Validators.required]
      }),
      subActividad: new FormControl('', {
        validators: [Validators.required]
      }),
      resultadoObjetivo: new FormControl('', {
        validators: [Validators.required]
      }),
      subIndicadores: new FormControl('', {
        validators: [Validators.required]
      }),
      subMediosVerificacion: new FormControl('', {
        validators: [Validators.required]
      }),
      presupuesto: new FormControl('', {
        validators: [Validators.required]
      }),
      encargado: new FormControl('', {
        validators: [Validators.required]
      })
    });
  }

  obtenerToken(): Token{
    return this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  obtenerCodigosObjetivos(): void {
    this.usuario = this.obtenerToken();
    const area = this.usuario.area;
    const gestion = this.usuario.gestion;
    const sede = this.usuario.sede;
    this.service.obtenerListaCodigosObjetivos(area, gestion, sede).subscribe(
      (response) => {
        let arreglo = [];
        for (let i = 0; i < response.length; i++) {
          const element = response[i];
          arreglo['codigo'] = element[0];
          arreglo['objetivo'] = element[1].substring(0, 40) + '...';
          this.listaCodigos.push(arreglo);
          arreglo = [];
        }
      }, (err) => {
        console.log('No hay objetivos registrados');
        console.log(err);
      }
    );
  }

  obtenerCodObj(codigoSeleccionada): void{
    const codigo = codigoSeleccionada.codigo;
  }

  public cerrarModal(): any {
    this.service.cerrarModal();
  }

  public guardarSubActividad(): void{
    const formSubAct = this.formSubActividad.value;
    this.subActividad.fechaInicio = formSubAct.fechaInicio;
    this.subActividad.fechaFin = formSubAct.fechaFin;
    this.subActividad.estado = true;
    this.subActividad.subActividad = formSubAct.subActividad;
    this.subActividad.resultadoObjetivo = formSubAct.resultadoObjetivo;
    this.subActividad.subIndicador = formSubAct.subIndicadores;
    this.subActividad.subMediosVerificacion = formSubAct.subMediosVerificacion;
    this.subActividad.presupuesto = formSubAct.presupuesto;
    this.subActividad.encargado = formSubAct.encargado.completo;
    this.listaSubAct.push(this.subActividad);
    this.subActividad = new SubActividad();
    this.formSubActividad.reset();
    this.agregar = false;
  }

  cancelarSubActividad(): void {
    if (this.agregar === true) {
      this.agregar = false;
      this.formSubActividad.reset();
    }
  }

  quitarSubActividad(subAct: SubActividad): void {
      this.listaSubAct = this.listaSubAct.filter(act => act !== subAct);
  }

  editarSubActividad(subAct): void {
    this.agregar = true;
    this.formSubActividad = new FormGroup({
      fechaInicio: new FormControl(subAct.fechaInicio, {
        validators: [Validators.required]
      }),
      fechaFin: new FormControl(subAct.fechaFin, {
        validators: [Validators.required]
      }),
      subActividad: new FormControl(subAct.subActividad, {
        validators: [Validators.required]
      }),
      resultadoObjetivo: new FormControl(subAct.resultadoObjetivo, {
        validators: [Validators.required]
      }),
      subIndicadores: new FormControl(subAct.subIndicador, {
        validators: [Validators.required]
      }),
      subMediosVerificacion: new FormControl(subAct.subMediosVerificacion, {
        validators: [Validators.required]
      }),
      presupuesto: new FormControl(subAct.presupuesto, {
        validators: [Validators.required]
      }),
      encargado: new FormControl('', {
        validators: [Validators.required]
      })
    });
    this.listaSubAct = this.listaSubAct.filter(act => act !== subAct);
  }

  obtenerListaPersonales(): void {
    //this.data$ = this.service.obtenerListaPersonales();

    this.service.obtenerListaPersonales().subscribe(
      (response) => {

        this.data$ = response;

    }, (err) => {
      //console.log('No hay objetivos registrados');
      console.log(err);
    });
  }

  guardarActividad(): void {
    this.formActividad.value.subActividad = this.listaSubAct;

    this.actividades.codObjetivo = this.formActividad.value.codObjetivo.codigo;
    this.actividades.actividad = this.formActividad.value.actividad;
    this.actividades.resultado = this.formActividad.value.resultado;
    this.actividades.estado = true;
    this.actividades.fechaIngresada = new Date();
    this.actividades.fechaActualizada = new Date();
    this.actividades.subActividades = this.listaSubAct;
    //console.log(this.actividades);
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    const area = this.usuario.area;
    const sede = this.usuario.sede;

    this.service.resgistrarActividades(this.actividades, area, sede).subscribe(
    (response) => {

      Swal.fire({
        position: 'center',
        icon: 'success',
        title: `${response.mensaje}`,
        showConfirmButton: false,
        timer: 2000
      });

      this.formActividad.reset();
      this.formSubActividad.reset();
      this.actividades = new Actividad();
      this.listaSubAct = new Array<SubActividad>();
      this.service.cerrarModal();

    }, (err) => {
      console.log(err)
    }
  );

  }
}
