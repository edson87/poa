package esam.edu.bo.poabackend.web.controllers;


import esam.edu.bo.poabackend.services.*;
import esam.edu.bo.poabackend.web.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.util.*;

@CrossOrigin
@RequestMapping("/api")
@RestController
public class ObjetivosController {

    @Autowired(required=true)
    ObjetivoSeviceImpl objetivosServiceImp;

    @Autowired(required=true)
    CronogramaServiceImpl cronogramaService;

    @Autowired
    ActividadServicesImp actividadServices;

    @Autowired
    PermisoImplServ permisoImpl;

    @Autowired
    UsuarioImplServ usuarioImpl;

    @Autowired
    SubActividadServiceImp subActividadService;

    @GetMapping("/objetivos")
    public ResponseEntity<?> getAllObjetivos() {
        List<Objetivo> lista = objetivosServiceImp.getAllObjetivos();

        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping("/objetivos")
    public ResponseEntity<?> saveObjetivos(@RequestBody Objetivo objetivo) {
        Map<String, Object> message = new HashMap<>();
        Objetivo newObjetivo = new Objetivo();
        List<CronogramaAnual> meses = new ArrayList<>();
        CronogramaAnual mes = new CronogramaAnual();
        Usuario usuario = new Usuario();

        Usuario usuarioGuardado = new Usuario();
        Permiso permisos = new Permiso();
        Permiso existePermisos = new Permiso();

        if (objetivo == null) {
            message.put("mensaje", "Los datos del objetivo estan vacios.");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try{
            existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());

            if (existePermisos == null) {
                usuario = objetivo.getUsuario();

                permisos = usuario.getPermisos();

                usuario.setPermisos(null);
                usuario.setObjetivo(null);

                usuarioGuardado = usuarioImpl.insertUsuario(usuario);

                permisos.setUsuario(usuarioGuardado);
                permisoImpl.savePermisos(permisos);
            } else {
               usuarioGuardado = existePermisos.getUsuario();
            }

            meses = objetivo.getMeses();

            objetivo.setMeses(null);
            objetivo.setUsuario(usuarioGuardado);

            newObjetivo = objetivosServiceImp.saveObjetivos(objetivo);


            for (int i = 0; i < meses.size(); i++) {
                mes = meses.get(i);
                mes.setObjetivo(newObjetivo);
                cronogramaService.saveInObjetive(mes);
            }

            /*existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());

            if (existePermisos == null) {

                    usuario.setObjetivo(newObjetivo);

                    usuarioGuardado = usuarioImpl.insertUsuario(usuario);


                    permisos.setUsuario(usuarioGuardado);
                    permisoImpl.savePermisos(permisos);
            }*/


        }catch (DataAccessException e) {
            message.put("mensaje","No se pudo guardar el Objetivo de la gestion".concat(objetivo.getGestion()));
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "EL objetivo se guardo correctamente.");

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/objetivos/{id}/{estado}")
    public ResponseEntity<?> getObjetivos(@PathVariable(name = "id") Long id, @PathVariable(name = "estado") boolean estado) {
        Objetivo objetivo = new Objetivo();
        Map<String, Object> message = new HashMap<>();

        if (id == null) {
            message.put("mensaje", "El id enviado no puede estar vacio.");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {
            objetivo = objetivosServiceImp.getObjetivos(id, estado);

            if (objetivo == null) {
                message.put("mensaje", "No existe ese objetivo.");
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } catch (DataAccessException e) {
            message.put("mensaje", "Ocurrio un error al obtener el objetivo.");
            message.put("error", e.getCause().getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(objetivo, HttpStatus.OK);
    }

    @PutMapping("/objetivos/{id}")
    public ResponseEntity<?> updateObjetivos(@PathVariable(name = "id") long id, @RequestBody Objetivo objetivos) {
        Objetivo objetivoSeleccionado = new Objetivo();
        Objetivo objetivoActualizado = new Objetivo();
        List<CronogramaAnual> listaCronograma = new ArrayList<>();
        CronogramaAnual cronogramaAnual = new CronogramaAnual();
        Map<String, Object> message = new HashMap<>();

        listaCronograma = objetivos.getMeses();
        objetivoSeleccionado = objetivosServiceImp.getObjetivos(id, objetivos.isEstado());
        objetivoSeleccionado.setGestion(objetivos.getGestion());
        objetivoSeleccionado.setArea(objetivos.getArea());
        objetivoSeleccionado.setSede(objetivos.getSede());
        objetivoSeleccionado.setCodigoobjetivo(objetivos.getCodigoobjetivo());
        objetivoSeleccionado.setObjetivogestion(objetivos.getObjetivogestion());
        objetivoSeleccionado.setProductoresultadoobjetivo(objetivos.getProductoresultadoobjetivo());
        objetivoSeleccionado.setIndicadorobjetivo(objetivos.getIndicadorobjetivo());
        objetivoSeleccionado.setMedioverificacionobjetivo(objetivos.getMedioverificacionobjetivo());
        objetivoSeleccionado.setEstado(objetivos.isEstado());
        objetivoSeleccionado.setMeses(null);
        objetivoSeleccionado.setFechahoraregistro(objetivos.getFechahoraregistro());
        objetivoSeleccionado.setCodigousuario(objetivos.getCodigousuario());
        objetivoActualizado = objetivosServiceImp.updateObjetivos(objetivoSeleccionado);


        cronogramaService.delateCronogramasById(objetivoSeleccionado);

        for (int i = 0; i < listaCronograma.size(); i++){
            cronogramaAnual = listaCronograma.get(i);
            cronogramaAnual.setObjetivo(objetivoActualizado);
            cronogramaService.saveInObjetive(cronogramaAnual);
        }

        message.put("mensaje","Se actualizo correctamente el Objetivo.");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    /*@DeleteMapping("/objetivos/{id}")
    public void deleteObjetivos(@PathVariable(name = "id") long id) {
        objetivosServiceImp.deleteObjetivos(id);
    }*/

    @PutMapping("/objetivos/eliminar")
    public ResponseEntity<?> eliminacionLogica(@RequestBody Objetivo objetivo) {
        Map<String, Object> messages = new HashMap<>();
        Objetivo objetivoSeleccionado = new Objetivo();
        List<Actividad> actividadSeleccionada = new ArrayList<>();
        try {
            objetivoSeleccionado = objetivosServiceImp.getObjetivos(objetivo.getId(), objetivo.isEstado());
            objetivoSeleccionado.setEstado(false);

            actividadSeleccionada = objetivoSeleccionado.getActividades();

            actividadSeleccionada.stream().forEach((a) -> {
                a.setEstado(false);
                actividadServices.saveActividad(a);
            });

            objetivosServiceImp.saveObjetivos(objetivoSeleccionado);

        }catch (DataAccessException e) {
            messages.put("mensaje", "Ocurrio un error al eliminar el objetivo");
            messages.put("error", e.getMessage());
        }
        messages.put("mensaje", "Se elimino satisfactoriamente");

        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping("/objetivos/managements/{area}/{estado}")
    public ResponseEntity<?> selectAllManagements(@PathVariable("area") String area, @PathVariable("estado") boolean estado) {
        List<String> listOfManagements = objetivosServiceImp.getAllManagements(area, estado);
        Map<String, Object> messages = new HashMap<>();

        if (listOfManagements == null) {
            messages.put("mensaje", "No hay gestiones guardadas.");
            return new ResponseEntity<>(messages, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(listOfManagements, HttpStatus.OK);
    }

    @GetMapping("/objetivos/{gestion}/{area}/{estado}/{sede}")
    public ResponseEntity<?> getObjetivo(@PathVariable("gestion") String gestion, @PathVariable("area") String area,
                                         @PathVariable("estado") boolean estado, @PathVariable("sede") String sede) {
        List<Objetivo> objetivo = new ArrayList<>();
        Map<String, Object> message = new HashMap<>();
        List<Actividad>listaAct = new ArrayList<>();
        List<SubActividad> listaSubActi = new ArrayList<>();

        if (gestion.equals("") && area.equals("")){
            message.put("mensaje", "Una de las variables area o gestion esta vacía.");
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }

        try {
            List<Actividad> listaActiviades = new ArrayList<>();
            
            objetivo = objetivosServiceImp.getObjetiveByManagementAndAreaAndSede(gestion, area, estado, sede);
            
            for (int i = 0; i < objetivo.size(); i++){
                listaActiviades = objetivo.get(i).getActividades();
                
                for (int j = 0; j < listaActiviades.size(); j++) {
                    Actividad act = listaActiviades.get(j);

                    if (act.isEstado() == true) {
                        //listaAct.add(act);

                        for (int k = 0; k < act.getSubActividades().size(); k++) {
                            SubActividad s = act.getSubActividades().get(k);

                            if (s.isEstado() == true) {
                                listaSubActi.add(s);
                            }
                        }
                        act.setSubActividades(listaSubActi);
                        listaSubActi = new ArrayList<>();
                        listaAct.add(act);
                    }

                }
                objetivo.get(i).setActividades(listaAct);
                listaAct = new ArrayList<>();
            }

            if(objetivo == null) {
                message.put("mensaje","No hay registros");
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }

        } catch (DataAccessException e){
            message.put("mensaje","Error al obtener el Objetivo de la gestion".concat(gestion));
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(objetivo, HttpStatus.OK);
    }

    @PostMapping("/objetivos/copiar/{sede}")
    public ResponseEntity<?> copiarObjetivo(@PathVariable(name = "sede") String sede, @RequestBody Objetivo objetivo) {
        Map<String, Object> message = new HashMap<>();
        List<SubActividad> subActividades = new ArrayList<>();
        List<Actividad> actividades = new ArrayList<>();
        List<CronogramaAnual> meses = new ArrayList<>();
        Usuario usuario = new Usuario();
        Permiso permisos = new Permiso();
        Objetivo newObjetivo = new Objetivo();

        if (sede == null) {
            message.put("mensaje", "La sede enviada esta vacia");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {

            Permiso existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());
            Usuario usuarioGuardado = new Usuario();


            if (existePermisos == null) {
                usuario = objetivo.getUsuario();
                permisos = usuario.getPermisos();

                usuario.setId(null);
                usuario.setPermisos(null);
                usuario.setObjetivo(null);

                permisos.setId(null);

                usuarioGuardado = usuarioImpl.insertUsuario(usuario);

                permisos.setUsuario(usuarioGuardado);
                permisoImpl.savePermisos(permisos);
            }else{
                usuarioGuardado = existePermisos.getUsuario();
            }


            meses = objetivo.getMeses();

            actividades = objetivo.getActividades();

            objetivo.setMeses(null);
            objetivo.setUsuario(usuarioGuardado);
            objetivo.setActividades(null);
            objetivo.setCodigousuario(objetivo.getCodigousuario());
            objetivo.setId(null);
            objetivo.setSede(sede);
            objetivo.setObjetivogestion(objetivo.getObjetivogestion().concat("(copia)"));

            newObjetivo = objetivosServiceImp.saveObjetivos(objetivo);

            CronogramaAnual mes = new CronogramaAnual();
            for (int i = 0; i < meses.size(); i++) {
                mes = meses.get(i);
                mes.setId(null);
                mes.setObjetivo(newObjetivo);
                cronogramaService.saveInObjetive(mes);
            }

            //Permiso existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());
           /* Usuario usuarioGuardado = new Usuario();
            //if (existePermisos == null) {

                usuario.setObjetivo(newObjetivo);

                usuarioGuardado = usuarioImpl.insertUsuario(usuario);


                permisos.setUsuario(usuarioGuardado);
                permisoImpl.savePermisos(permisos);
            //}
            */

            for (int i = 0; i < actividades.size(); i++) {
                Actividad a = actividades.get(i);

                subActividades = a.getSubActividades();
                a.setSubActividades(null);
                a.setId(null);
                a.setActividad(a.getActividad().concat("(copia)"));
                Actividad newActividad = actividadServices.saveActividad(a);

                for (int j = 0; j < subActividades.size(); j++) {
                    SubActividad s = subActividades.get(j);
                    s.setId(null);
                    s.setActividad(newActividad);
                    subActividadService.saveSubActividad(s);
                }

                /*String codObjetive = newActividad.getCodObjetivo();
                String area = objetivo.getArea();
                boolean estado = true;

                objetivo = objetivosServiceImp.getObjetiveByCodObjeAndAreaAndSede(codObjetive, area, estado, sede);*/

                newActividad.setObjetivo(newObjetivo);
                actividadServices.saveActividad(newActividad);
            }



        } catch (DataAccessException e) {
            message.put("mensaje", "Hubo un error copiar el objetivo");
            message.put("error", e.getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "Se copio exitosamente el objetivo");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }


    @PostMapping("/objetivos/copiar-todos/{sede}")
    public ResponseEntity<?> copiarTodosObjetivos(@PathVariable(name = "sede") String sede, @RequestBody List<Objetivo> objetivos) {
        Map<String, Object> message = new HashMap<>();
        List<SubActividad> subActividades = new ArrayList<>();
        List<Actividad> actividades = new ArrayList<>();
        List<CronogramaAnual> meses = new ArrayList<>();
        Usuario usuario = new Usuario();
        Permiso permisos = new Permiso();
        Objetivo newObjetivo = new Objetivo();

        if (sede == null) {
            message.put("mensaje", "La sede enviada esta vacia");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {
            for (Objetivo objetivo: objetivos ) {

                Permiso existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());
                Usuario usuarioGuardado = new Usuario();
                if (existePermisos == null) {
                    usuario = objetivo.getUsuario();
                    permisos = usuario.getPermisos();

                    usuario.setId(null);
                    usuario.setPermisos(null);
                    usuario.setObjetivo(null);

                    permisos.setId(null);

                    usuarioGuardado = usuarioImpl.insertUsuario(usuario);

                    permisos.setUsuario(usuarioGuardado);
                    permisoImpl.savePermisos(permisos);
                }else{
                    usuarioGuardado = existePermisos.getUsuario();
                }

                meses = objetivo.getMeses();

                actividades = objetivo.getActividades();

                objetivo.setMeses(null);
                objetivo.setUsuario(usuarioGuardado);
                objetivo.setActividades(null);
                objetivo.setCodigousuario(objetivo.getCodigousuario());
                objetivo.setId(null);
                objetivo.setSede(sede);
                objetivo.setObjetivogestion(objetivo.getObjetivogestion().concat("(copia)"));

                newObjetivo = objetivosServiceImp.saveObjetivos(objetivo);

                CronogramaAnual mes = new CronogramaAnual();
                for (int i = 0; i < meses.size(); i++) {
                    mes = meses.get(i);
                    mes.setId(null);
                    mes.setObjetivo(newObjetivo);
                    cronogramaService.saveInObjetive(mes);
                }

                for (int i = 0; i < actividades.size(); i++) {
                    Actividad a = actividades.get(i);

                    subActividades = a.getSubActividades();
                    a.setSubActividades(null);
                    a.setId(null);
                    a.setActividad(a.getActividad().concat("(copia)"));
                    Actividad newActividad = actividadServices.saveActividad(a);

                    for (int j = 0; j < subActividades.size(); j++) {
                        SubActividad s = subActividades.get(j);
                        s.setId(null);
                        s.setActividad(newActividad);
                        subActividadService.saveSubActividad(s);
                    }

                    newActividad.setObjetivo(newObjetivo);
                    actividadServices.saveActividad(newActividad);
                }

            }

        } catch (DataAccessException e) {
            message.put("mensaje", "Hubo un error copiar el objetivo");
            message.put("error", e.getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "Se copio exitosamente el objetivo");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }


/*
    @PostMapping("/objetivos/copiar/gestion/{gestion}")
    public ResponseEntity<?> copiarObjetivoPorAño(@PathVariable(name = "gestion") String gestion, @RequestBody Objetivo objetivo) {
        Map<String, Object> message = new HashMap<>();
        List<SubActividad> subActividades = new ArrayList<>();
        List<Actividad> actividades = new ArrayList<>();
        List<CronogramaAnual> meses = new ArrayList<>();
        Usuario usuario = new Usuario();
        Permiso permisos = new Permiso();
        Objetivo newObjetivo = new Objetivo();

        if (gestion == null) {
            message.put("mensaje", "La gestion enviada esta vacia");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {

            Permiso existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());
            Usuario usuarioGuardado = new Usuario();
            if (existePermisos == null) {
                usuario = objetivo.getUsuario();
                permisos = usuario.getPermisos();

                usuario.setId(null);
                usuario.setPermisos(null);
                usuario.setObjetivo(null);

                permisos.setId(null);

                usuarioGuardado = usuarioImpl.insertUsuario(usuario);

                permisos.setUsuario(usuarioGuardado);
                permisoImpl.savePermisos(permisos);
            }else{
                usuarioGuardado = existePermisos.getUsuario();
            }


            meses = objetivo.getMeses();

            actividades = objetivo.getActividades();

            objetivo.setMeses(null);
            objetivo.setUsuario(usuarioGuardado);
            objetivo.setActividades(null);
            objetivo.setCodigousuario(objetivo.getCodigousuario());
            objetivo.setId(null);
            objetivo.setGestion(gestion);
            objetivo.setObjetivogestion(objetivo.getObjetivogestion().concat("(copia)"));

            newObjetivo = objetivosServiceImp.saveObjetivos(objetivo);

            CronogramaAnual mes = new CronogramaAnual();
            for (int i = 0; i < meses.size(); i++) {
                mes = meses.get(i);
                mes.setId(null);
                mes.setObjetivo(newObjetivo);
                cronogramaService.saveInObjetive(mes);
            }

            //Permiso existePermisos = permisoImpl.getPermisos(objetivo.getCodigousuario());
           // Usuario usuarioGuardado = new Usuario();
            //if (existePermisos == null) {

           //     usuario.setObjetivo(newObjetivo);

           //     usuarioGuardado = usuarioImpl.insertUsuario(usuario);


           /     permisos.setUsuario(usuarioGuardado);
            //    permisoImpl.savePermisos(permisos);
            //}


            for (int i = 0; i < actividades.size(); i++) {
                Actividad a = actividades.get(i);

                subActividades = a.getSubActividades();
                a.setSubActividades(null);
                a.setId(null);
                a.setActividad(a.getActividad().concat("(copia)"));
                Actividad newActividad = actividadServices.saveActividad(a);

                for (int j = 0; j < subActividades.size(); j++) {
                    SubActividad s = subActividades.get(j);
                    s.setId(null);
                    s.setActividad(newActividad);
                    subActividadService.saveSubActividad(s);
                }

                newActividad.setObjetivo(newObjetivo);
                actividadServices.saveActividad(newActividad);
            }



        } catch (DataAccessException e) {
            message.put("mensaje", "Hubo un error copiar el objetivo");
            message.put("error", e.getMessage());
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "Se copio exitosamente el objetivo");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
*/

}
