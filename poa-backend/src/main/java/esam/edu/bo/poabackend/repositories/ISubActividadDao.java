package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Actividad;
import esam.edu.bo.poabackend.web.models.SubActividad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ISubActividadDao extends JpaRepository<SubActividad, Long> {

    public void deleteSubActividadByActividad(Actividad actividad);

    @Query("select s from SubActividad s where s.actividad = ?1 and s.estado = true")
    public List<SubActividad> findSubActividadesByActividad(Actividad actividad);
}
