package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IUsuarioDao extends JpaRepository<Usuario, Long> {

    public Usuario findUsuarioByCodigousuario(String cod_usuario);
}
