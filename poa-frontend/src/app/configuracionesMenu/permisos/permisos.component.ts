import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { ConfiguracionService } from '../../configuracionService/configuracion.service';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.css']
})
export class PermisosComponent implements OnInit {

  public keyword = 'rol';
  public data$: Observable<any>;
  public permisos: any = {};

  formPermisos: FormGroup;



  constructor(private service: ConfiguracionService) { }

  ngOnInit(): void {
    //this.getAllUsuarios();
    this.getAllRoles();
    this.iniciarFormCodUsuario();
  }

  public getAllRoles(): void {
    this.service.getAllRoles().subscribe(
      (response) => {
        this.data$ = response;
      }
    )
  }

  /*public getAllUsuarios(): void {
    this.service.getCodAllUsuario().subscribe(
      (response) => {
        //this.data$ = response;
        console.log(response)
      }
    )
  }*/


  selectEvent(event): void {
    this.permisos = event;

  }

  searchCleared() {
    this.permisos = {};
    this.formPermisos.reset;
  }

  iniciarFormCodUsuario(): void{
      this.formPermisos = new FormGroup({
        codigousuario: new FormControl('', {
          validators: []
        })
      })
  }

  guardarPermisos() {
    const permisos = this.permisos
    //const cod_usuario = this.formPermisos.value.codigousuario.codigousuario.toString();
    permisos.usuario = null;
    console.log(permisos)
    this.service.updateRol(permisos).subscribe(
      (response) => {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `${response.mensaje}`,
          showConfirmButton: false,
          timer: 1500
        });
        this.ngOnInit();
        this.permisos = {};
      }, (err) => {
        console.log(err)
      }
    )
    /*this.service.editarPermisosUsuario(permisos, cod_usuario).subscribe(
      (response) => {

        Swal.fire({
          position: 'center',
          icon: 'success',
          title: `${response.mensaje}`,
          showConfirmButton: false,
          timer: 1500
        });
        this.ngOnInit();
        this.permisos = {};
      }, (err) => {
        console.log(err)
      }
    )*/
  }

}
