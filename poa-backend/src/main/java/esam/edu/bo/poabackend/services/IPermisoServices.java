package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Permiso;

import java.util.List;

public interface IPermisoServices {
    public List<Permiso> getAllPermisos();
    public List<Permiso> getPermisosByRol(String rol);
    public void updatePermisoByRol(Permiso permiso);
    public Permiso getPermisos(String cod_usuario);
    public Permiso savePermisos(Permiso permiso);
    public Permiso updatePermisos(Permiso permiso);
}
