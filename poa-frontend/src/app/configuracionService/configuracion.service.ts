import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Token } from '../Modelos/token';
import { Observable } from 'rxjs';
import { URL_BACKEND } from '../config_url/config';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionService {
  private menu = new BehaviorSubject<any>(undefined);
  currentmenu = this.menu.asObservable();

  private menu2 = new BehaviorSubject<any>(undefined);
  currentmenu2 = this.menu2.asObservable();

  private menuSede = new BehaviorSubject<any>(undefined);
  currentmenuSede = this.menuSede.asObservable();

  //private urlPermisos = 'http://localhost:8080/api/usuarios';
  private urlPermisos = URL_BACKEND + '/api/usuarios';

  private urlPermiso = URL_BACKEND + '/api/permisos'

  constructor(private http: HttpClient) { }


  public mandarMensaje(token: Token): void {
    this.menu.next(token);
  }

  public mandarMensajeDesdeObjetivo(token: Token): void {
    this.menu2.next(token);
  }

  public mandarMensajeCambioSede(token: Token): void {
    this.menuSede.next(token);
  }

/*  public getCodAllUsuario(): Observable<any> {
    return this.http.get<any>(`${this.urlPermisos}`);
  }

  public editarPermisosUsuario(permisos: any, codUsuario: string): Observable<any> {
    return this.http.put<any>(`${this.urlPermisos}/update/usuario/${codUsuario}`, permisos)
  }*/

  public getAllRoles(): Observable<any> {
    return this.http.get<any>(`${this.urlPermiso}`);
  }

  public updateRol(permiso: any): Observable<any> {
    return this.http.put<any>(`${this.urlPermiso}/rol`, permiso);
  }
}
