package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.IActividadDao;
import esam.edu.bo.poabackend.web.models.Actividad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActividadServicesImp implements IActividadServices {

    @Autowired
    IActividadDao actividadDao;


    @Override
    @Transactional(readOnly = true)
    public List<Actividad> getAllActivities() {
        List<Actividad> lista = actividadDao.findAll();
        return lista;
    }

    @Override
    public Actividad saveActividad(Actividad actividad) {
        return actividadDao.save(actividad);
    }

    @Override
    public Actividad getActivityById(Long id) {
        return actividadDao.findById(id).get();
    }
}
