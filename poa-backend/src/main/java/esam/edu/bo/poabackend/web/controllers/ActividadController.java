package esam.edu.bo.poabackend.web.controllers;

import esam.edu.bo.poabackend.repositories.IActividadDao;
import esam.edu.bo.poabackend.services.ActividadServicesImp;
import esam.edu.bo.poabackend.services.ObjetivoSeviceImpl;
import esam.edu.bo.poabackend.services.SubActividadServiceImp;
import esam.edu.bo.poabackend.web.models.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@CrossOrigin
@RequestMapping("/api/actividad")
@RestController
public class ActividadController {

    @Autowired
    public ActividadServicesImp actividadServices;

    @Autowired
    public SubActividadServiceImp subActividadServiceImp;

    @Autowired
    public ObjetivoSeviceImpl objetivoSevice;

    @Autowired
    public IActividadDao actividadDao;

    @GetMapping("/all")
    public List<Actividad> getAllActivities() {
        List<Actividad> activities = actividadServices.getAllActivities();
        return activities;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActivity(@PathVariable("id") Long id) {
        Map<String, Object> message = new HashMap<>();
        Actividad actividad = new Actividad();
        List<SubActividad> subActividades = new ArrayList<>();

        if (id == null) {
            message.put("mensaje", "El Id enviado esta vacio");

            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {
            actividad = actividadServices.getActivityById(id);

            for (int i = 0; i < actividad.getSubActividades().size(); i++) {
                SubActividad s = actividad.getSubActividades().get(i);
                if (s.isEstado() == true) {
                    subActividades.add(s);
                }
            }

            actividad.setSubActividades(subActividades);

            if (actividad == null) {
                System.out.println(actividad);
            }

        }catch (DataAccessException e) {
            message.put("mensaje", "Existio un error al obtener la actividad");
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(actividad, HttpStatus.OK);
    }

    @PostMapping("/guardar/{area}/{estado}/{sede}")
    public ResponseEntity<?> saveActividad(@RequestBody Actividad actividad, @PathVariable("area") String area,
                                           @PathVariable("estado") boolean estado, @PathVariable("sede") String sede) {
        Map<String, Object> message = new HashMap<>();
        List<SubActividad> listSubActi = new ArrayList<>();
        Actividad newActividad = new Actividad();
        SubActividad newSubActividad = new SubActividad();
        Objetivo objetivo = new Objetivo();
        List<Actividad> actividades = new ArrayList<>();
        Objetivo objetivo1 = new Objetivo();
        Encargado encargado = new Encargado();

        if (actividad == null) {
            message.put("mensaje", "La activdad no puede ser nula");
            return new ResponseEntity<Map<String, Object>>(message, HttpStatus.BAD_REQUEST);
        }

        try {

                listSubActi = actividad.getSubActividades();
                actividad.setSubActividades(null);


                newActividad =  actividadServices.saveActividad(actividad);

                for (int i = 0; i < listSubActi.size(); i++) {
                    newSubActividad = listSubActi.get(i);
                    newSubActividad.setActividad(newActividad);


                    SubActividad acti = subActividadServiceImp.saveSubActividad(newSubActividad);

                }

                String codObjetive = actividad.getCodObjetivo();
                objetivo = objetivoSevice.getObjetiveByCodObjeAndAreaAndSede(codObjetive, area, estado, sede);

                newActividad.setObjetivo(objetivo);

                actividadServices.saveActividad(actividad);

        } catch (DataAccessException ex) {
            message.put("mensaje", "Hubo un error al guardar la actividad");
            message.put("error", ex.getCause().getMessage());
        }

        message.put("mensaje","La actividad se guardo con exito.");
        return new ResponseEntity<Map<String, Object>>(message, HttpStatus.OK);
    }

    @GetMapping("/{codObje}/{area}/{estado}/{sede}")
    public ResponseEntity<?> getActividadByAreaAndManageManagement(@PathVariable("codObje") String codObje,
                                                                   @PathVariable("area") String area,
                                                                   @PathVariable("estado") boolean estado,
                                                                   @PathVariable("sede") String sede) {
        Map<String, Object> message = new HashMap<>();
        Objetivo objetivo = new Objetivo();
        List<Actividad> actividad = new ArrayList<>();
        List<SubActividad> subActividades = new ArrayList<>();

        if (codObje.equals("") && area.equals("")){
            message.put("mensaje", "Una de las variables area o gestion esta vacía.");
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }

        try {
            objetivo = objetivoSevice.getObjetiveByCodObjeAndAreaAndSede(codObje, area, estado, sede);
            //actividad = objetivo.getActividades();

            for (int i = 0; i < objetivo.getActividades().size(); i++){
                if (objetivo.getActividades().get(i).isEstado() == true) {

                    for (int j = 0; j < objetivo.getActividades().get(i).getSubActividades().size(); j++) {
                        SubActividad s = objetivo.getActividades().get(i).getSubActividades().get(j);
                        if (s.isEstado() == true) {
                            subActividades.add(s);
                        }
                    }
                    objetivo.getActividades().get(i).setSubActividades(subActividades);
                    actividad.add(objetivo.getActividades().get(i));
                    subActividades = new ArrayList<>();
                }
            }



            if(objetivo == null) {
                message.put("mensaje","No hay registros");
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }

        } catch (DataAccessException e) {
            message.put("mensaje", "Error al obtener las Actividades de la gestion".concat(codObje));
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(actividad, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<?> updateActivity(@RequestBody Actividad actividad) {
        Map<String, Object> message = new HashMap<>();
        List<SubActividad> listSubActi = new ArrayList<>();
        Actividad actualActividad = new Actividad();
        Actividad nuevaActividad = new Actividad();
        SubActividad newSubActividad = new SubActividad();

        if (actividad == null) {
            message.put("mensaje", "La actividad esta vacia.");
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }

        try {
            actualActividad = actividadServices.getActivityById(actividad.getId());

            actualActividad.setActividad(actividad.getActividad());
            actualActividad.setResultado(actividad.getResultado());
            actualActividad.setFechaActualizada(actividad.getFechaActualizada());

            listSubActi = actividad.getSubActividades();

            nuevaActividad = actividadServices.saveActividad(actualActividad);

            List<SubActividad> subActividadAntigua = subActividadServiceImp.findAllSubActividadesInTrue(nuevaActividad);

            for (int i = 0; i < subActividadAntigua.size(); i++) {

                for (int j = 0; j < listSubActi.size(); j++) {

                    if (subActividadAntigua.get(i).getId() == listSubActi.get(j).getId() && listSubActi.get(j).isEstado() == true) {
                        subActividadAntigua.get(i).setFechaInicio(listSubActi.get(j).getFechaInicio());
                        subActividadAntigua.get(i).setFechaFin(listSubActi.get(j).getFechaFin());
                        subActividadAntigua.get(i).setEstado(true);
                        subActividadAntigua.get(i).setSubActividad(listSubActi.get(j).getSubActividad());
                        subActividadAntigua.get(i).setResultadoObjetivo(listSubActi.get(j).getResultadoObjetivo());
                        subActividadAntigua.get(i).setSubIndicador(listSubActi.get(j).getSubIndicador());
                        subActividadAntigua.get(i).setSubMediosVerificacion(listSubActi.get(j).getSubMediosVerificacion());
                        subActividadAntigua.get(i).setPresupuesto(listSubActi.get(j).getPresupuesto());
                        subActividadAntigua.get(i).setEncargado(listSubActi.get(j).getEncargado());

                        newSubActividad = subActividadAntigua.get(i);
                        subActividadServiceImp.saveSubActividad(newSubActividad);
                    } else if(subActividadAntigua.get(i).getId() == listSubActi.get(j).getId() && listSubActi.get(j).isEstado() == false) {
                        subActividadAntigua.get(i).setEstado(false);
                        subActividadServiceImp.saveSubActividad(subActividadAntigua.get(i));

                    } else if (listSubActi.get(j).getId() == null) {
                        listSubActi.get(j).setActividad(nuevaActividad);
                        subActividadServiceImp.saveSubActividad(listSubActi.get(j));
                    }
                }
            }


            //subActividadServiceImp.deleteSubactividadByActividad(nuevaActividad);

            /*for (int i = 0; i < listSubActi.size(); i++) {
                newSubActividad = listSubActi.get(i);
                newSubActividad.setActividad(nuevaActividad);

                subActividadServiceImp.saveSubActividad(newSubActividad);
            }*/

        } catch (DataAccessException e) {
            message.put("mensaje", "Error al obtener las Actividades");
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        message.put("mensaje", "Actividad actualizada correctamente");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PutMapping("/eliminar")
    public ResponseEntity<?> deleteActivity(@RequestBody Actividad actividad) {
        Map<String, Object> message = new HashMap<>();
        Actividad actividadSeleccionada = new Actividad();

        try{

            actividadSeleccionada = actividadServices.getActivityById(actividad.getId());
            actividadSeleccionada.setEstado(false);

            actividadServices.saveActividad(actividadSeleccionada);
        }catch (DataAccessException e) {
            message.put("mensaje", "No se pudo eliinar la actividad");
            message.put("error", e.getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        message.put("mensaje", "Se elimino correctamente la actividad.");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/codObjetivo/area/{area}/gestion/{gestion}/{estado}/{sede}")
    public ResponseEntity<?> selectAllCodObjetives(@PathVariable("area") String area, @PathVariable("gestion") String gestion,
                                                   @PathVariable("estado") boolean estado, @PathVariable("sede") String sede) {
        Map<String, Object> message = new HashMap<>();
        List<Object[]> cods = new ArrayList<>();
        CodigoYObjetivo codigo = new CodigoYObjetivo();
        List<CodigoYObjetivo> lista = new ArrayList<>();

        if (area.equals("") && gestion.equals("")){
            message.put("mensaje","El area esta vacia.");
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        try {
            cods = objetivoSevice.getAllCodObjetivos(area, gestion, estado, sede);

        }catch (DataAccessException e){
            message.put("mensaje","Error al obtener los codigos de objetivos");
            message.put("error", e.getCause().getMessage());

            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(cods, HttpStatus.OK);
    }
}
