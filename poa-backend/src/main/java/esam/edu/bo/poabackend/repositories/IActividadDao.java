package esam.edu.bo.poabackend.repositories;

import esam.edu.bo.poabackend.web.models.Actividad;
import esam.edu.bo.poabackend.web.models.Objetivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IActividadDao extends JpaRepository<Actividad, Long> {
}
