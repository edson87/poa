import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetivoCopiarComponent } from './objetivo-copiar.component';

describe('ObjetivoCopiarComponent', () => {
  let component: ObjetivoCopiarComponent;
  let fixture: ComponentFixture<ObjetivoCopiarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetivoCopiarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetivoCopiarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
