package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.repositories.IPersonalDao;
import esam.edu.bo.poabackend.web.models.Personal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonalServImpl implements IPersonalServices {

    @Autowired
    private IPersonalDao personalDao;

    @Override
    @Transactional(readOnly = true)
    public List<Personal> getAllPersonals() {
        List<Personal> personales = personalDao.findAll();
        return personales;
    }
}
