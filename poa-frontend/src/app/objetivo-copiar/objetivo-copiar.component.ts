import { Component, OnInit, Input } from '@angular/core';
import { ObjetivoServicioService } from '../objetivo-servicios/objetivo-servicio.service';
import { Token } from '../Modelos/token';
import { sedes } from '../Modelos/sedes';
import  Swal  from 'sweetalert2';
import { Objetivo } from '../Modelos/Objetivo';


@Component({
  selector: 'app-objetivo-copiar',
  templateUrl: './objetivo-copiar.component.html',
  styleUrls: ['./objetivo-copiar.component.css']
})
export class ObjetivoCopiarComponent implements OnInit {

  @Input() listaCopiarObjetivo;

  public permisos: any;
  public usuario: Token;
  public listaSedes: any = sedes;
  public sedeSeleccionada: any;
  public anioPasar: any;

  constructor(public servicio: ObjetivoServicioService) { }

  ngOnInit(): void {
    this.obtenerGetionPorDefecto();
  }

  obtenerGetionPorDefecto(): void{
    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
  }

  obtenerPermisosUsuario(): void {
    const permiso = sessionStorage.getItem('permisos');
    this.permisos = JSON.parse(window.atob(permiso));
  }

  cerrarModalCopiar() :void{
    this.servicio.cerrarModalCopiar();
    this.sedeSeleccionada = null;
  }


  realizarCambioObjetivo(): void {
    const objetivo = this.listaCopiarObjetivo;
    const sede = this.sedeSeleccionada;
    let gestion = this.anioPasar;

    if (objetivo.length) {
      this.copiarTodosLosObjetivos(objetivo, gestion, sede);
    } else {
      this.copiarObjetivo(objetivo, gestion, sede);
    }

  }


  copiarTodosLosObjetivos(objetivos: Array<Objetivo>, gestion: any, sede: any) {
    this.servicio.copiarTodosLosObjetivo(objetivos, sede).subscribe(
      (response) => {
        Swal.fire({
           position: 'center',
           icon: 'success',
           title: `${response.mensaje}`,
           showConfirmButton: false,
           timer: 2000
         });
      }, (err) => {
        console.log(err.error);
        Swal.fire({
          icon: 'error',
          title: '',
          text: `${err.mensaje}`,
          footer: ''
        })
      }
    );
  }

  copiarObjetivo(objetivo: Objetivo, gestion: any, sede: any) {
    if (gestion === undefined) {
      this.servicio.copiarObjetivo(objetivo, sede).subscribe(
        (response) => {
          Swal.fire({
             position: 'center',
             icon: 'success',
             title: `${response.mensaje}`,
             showConfirmButton: false,
             timer: 2000
           });
        }, (err) => {
          console.log(err.error);
          Swal.fire({
            icon: 'error',
            title: '',
            text: `${err.mensaje}`,
            footer: ''
          })
        }
      );
    } else {
      this.servicio.copiarObjetivoPorAño(objetivo, gestion).subscribe(
        (response) => {
          Swal.fire({
             position: 'center',
             icon: 'success',
             title: `${response.mensaje}`,
             showConfirmButton: false,
             timer: 2000
           });
          console.log(response);
        }, (err) => {
          console.log(err.error);
          Swal.fire({
            icon: 'error',
            title: '',
            text: `${err.mensaje}`,
            footer: ''
          })
        }
      )
    }
  }

}
