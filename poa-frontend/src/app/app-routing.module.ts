import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';

import { InicioComponent } from './inicio/inicio.component';
import { ActividadRegistroComponent } from './actividad-registro/actividad-registro.component';
import { ObjetivoRegistroComponent } from './objetivo-registro/objetivo-registro.component';
import { ActividadCatalogoComponent } from './actividad-catalogo/actividad-catalogo.component';
import { NotFoundModule } from './not-found/not-found.module';
import { ObjetivoCatalogoComponent } from './objetivo-catalogo/objetivo-catalogo.component';
import { NotFoundComponent } from '../app/not-found/not-found.component';
import { ObjetivoEditarComponent } from './objetivo-editar/objetivo-editar.component';
import { ActividadEditarComponent } from './actividad-editar/actividad-editar.component';
import { GestionComponent } from './configuracionesMenu/gestion/gestion.component';
import { AreaComponent } from './configuracionesMenu/area/area.component';
import { PermisosComponent } from './configuracionesMenu/permisos/permisos.component';
import { VerObjetivoComponent } from './objetivo-ver/ver-objetivo/ver-objetivo.component';
import { SedeComponent } from './configuracionesMenu/sede/sede.component';

const routes: Routes = [
  {path: '', redirectTo: '/poa/:id', pathMatch: 'full'},
  {path: 'poa', component: InicioComponent },
  {path: 'poa/:id', component: InicioComponent },
  {path: 'objetivo/catalogo', component: ObjetivoCatalogoComponent},
  {path: 'objetivo/registro', component: ObjetivoRegistroComponent},
  {path: 'objetivo/editar/:id', component: ObjetivoEditarComponent},
  {path: 'objetivo/visualizar', component: VerObjetivoComponent},
  {path: 'actividad/registro', component: ActividadRegistroComponent},
  {path: 'actividad/catalogo', component: ActividadCatalogoComponent},
  {path: 'actividad/editar/:id', component: ActividadEditarComponent},
  {path: 'cambiar/gestiones', component: GestionComponent},
  {path: 'cambiar/area', component: AreaComponent},
  {path: 'cambiar/sede', component: SedeComponent},
  {path: 'cambiar/permisos', component: PermisosComponent},
  {path: '**', component: NotFoundComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
