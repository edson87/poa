import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetivoEditarComponent } from './objetivo-editar.component';

describe('ObjetivoEditarComponent', () => {
  let component: ObjetivoEditarComponent;
  let fixture: ComponentFixture<ObjetivoEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetivoEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetivoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
