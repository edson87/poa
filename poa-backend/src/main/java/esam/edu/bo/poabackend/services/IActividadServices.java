package esam.edu.bo.poabackend.services;

import esam.edu.bo.poabackend.web.models.Actividad;

import java.util.List;

public interface IActividadServices {
    public List<Actividad> getAllActivities();

    public Actividad saveActividad(Actividad actividad);

    public Actividad getActivityById(Long id);

}
