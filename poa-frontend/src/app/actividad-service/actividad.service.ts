import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Actividad } from '../Modelos/Actividad';
import { URL_BACKEND } from '../config_url/config';

@Injectable({
  providedIn: 'root'
})
export class ActividadService {
  //private urlApi = 'http://localhost:8080/api/actividad';
  //private urlApiPersonal = 'http://localhost:8080/api/personal';

  private urlApi = URL_BACKEND + '/api/actividad';
  private urlApiPersonal = URL_BACKEND + '/api/personal';

  public modal: boolean = false;

  constructor(private http: HttpClient) { }

  public obtenenerActividadPorCodObjeYAreaYSede(codObje: string, area: string, sede: string): Observable<any>{
    const estado = true;
    return this.http.get<any>(`${this.urlApi}/${codObje}/${area}/${estado}/${sede}`);
  }

  public resgistrarActividades(actividad: Actividad, area: string, sede: string): Observable<any> {
    const estado = true;
    return this.http.post<any>(`${this.urlApi}/guardar/${area}/${estado}/${sede}`, actividad);
  }

  public obtenerListaCodigosObjetivos(area: string, gestion: string, sede: string): Observable<any> {
    const estado = true;
    return this.http.get<any>(`${this.urlApi}/codObjetivo/area/${area}/gestion/${gestion}/${estado}/${sede}`);
  }

  public abrirModal(): boolean {
    return this.modal = true;
  }

  public cerrarModal(): boolean {
    return this.modal = false;
  }

  public obtenerListaPersonales(): Observable<any> {
    return this.http.get<any>(this.urlApiPersonal);
  }

  public eliminarActividad(actividad: Actividad): Observable<any> {
    return this.http.put<any>(`${this.urlApi}/eliminar`, actividad);
  }
}
