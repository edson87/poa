package esam.edu.bo.poabackend.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "permisos")
public class Permiso implements Serializable {
    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String rol;

    private boolean leer_objetivo;
    private boolean crear_objetivo;
    private boolean editar_objetivo;
    private boolean eliminar_objetivo;
    private boolean excel_objetivo;
    private boolean copiar_objetivo;

    private boolean leer_actividad;
    private boolean crear_actividad;
    private boolean editar_actividad;
    private boolean eliminar_actividad;

    @OneToOne
    /*@ManyToOne
    @JoinTable(name = "user_permiso",
            joinColumns = {@JoinColumn(name = "permiso_fk",
                    updatable = false, referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_fk",
                    referencedColumnName = "id")}
    )*/
    @JsonIgnoreProperties(value = "usuario")
    private Usuario usuario;

    /*@OneToOne
    @JsonIgnoreProperties(value = "rol")
    private Rol rol;*/

    /*@OneToOne()
    @JoinColumn(name = "cod_user", referencedColumnName = "codigousuario")
    @JsonIgnoreProperties(value = "objetivo")
    Objetivo objetivo;*/
}
